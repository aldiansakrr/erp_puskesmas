<!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-ambulance"></i>
                </div>
                <div class="sidebar-brand-text mx-3">SI APOTEK</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item <?php if($page=="Dashboard"){ echo "active"; } ?>">
                <a class="nav-link" href="index.php">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Interface
            </div>

            <!-- Nav Item - Data Obat -->
            <li class="nav-item <?php if($page=="Data Obat"){ echo "active"; } ?>">
                <a class="nav-link" href="dataobat.php">
                    <i class="fas fa-fw fa-capsules"></i>
                    <span>Obat</span></a>
            </li>
            
            <!-- Nav Item - Data Resep -->
            <li class="nav-item <?php if($page=="Resep Rawat Inap" || $page=="Resep Rawat Jalan" || $page=="Input Resep"){ echo "active"; } ?>">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseResep"
                    aria-expanded="true" aria-controls="collapseResep">
                    <i class="fas fa-fw fa-file-medical"></i>
                    <span>Resep</span>
                </a>
                <div id="collapseResep" class="collapse <?php if($page=="Resep Rawat Inap" || $page=="Resep Rawat Jalan"){ echo "show"; } ?> " aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Resep Obat</h6>
                        <a class="collapse-item <?php if($page=="Resep Rawat Inap"){ echo "active"; } ?> " href="v_resep_rawatinap.php">Resep Rawat Inap</a>
                        <a class="collapse-item <?php if($page=="Resep Rawat Jalan"){ echo "active"; } ?> " href="v_resep_rawatjalan.php">Resep Rawat Jalan</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Data Dokter -->
            <li class="nav-item <?php if($page=="Data Dokter"){ echo "active"; } ?>">
                <a class="nav-link" href="dokter.php">
                    <i class="fas fa-fw fa-user-md"></i>
                    <span>Dokter</span></a>
            </li>

          

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

           

        </ul>
        <!-- End of Sidebar -->