<?php
  $page = "Login";
  include 'config.php';

  if(isset($_POST['Login'])){
    session_start();
    $id_pegawai = $_POST['id_pegawai'];

  	// $login=mysqli_query($koneksi,"select * from admin where username='$username' and password='$password'");
  	$login=mysqli_query($koneksi,"select * from pegawai where id_pegawai='$id_pegawai'");
  	$cek = mysqli_num_rows($login);

  	if($cek >= 0){
  		$data = mysqli_fetch_assoc($login);

  		if(!empty($data['id_pegawai'])){
  			$_SESSION['id_pegawai'] = $data['id_pegawai'];
  			$_SESSION['nama_pegawai'] = $data['nama_pegawai'];
  			header("location:index.php");

  		} else {
  			header("location:login.php?pesan=gagal");
  		}
  	} else {
  		header("location:login.php?pesan=gagal");
  	}
  }

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $page ?> - Apotek</title>

    <?php include ('css.php'); ?>

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <!-- <div class="col-lg-6 d-none d-lg-block"></div> -->
                            <div class="col-lg-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                                    </div>
                                    <form class="user" action="" method="POST">
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-user"
                                                id="id_pegawai" aria-describedby="emailHelp"
                                                placeholder="Enter Email Address..." name="id_pegawai">
                                        </div>
                                        
                                        
                                        <button type="submit" name="Login" class="btn btn-primary btn-user btn-block">Login</button>
                                        
                                    </form>
                                    <!-- <hr>
                                    <div class="text-center">
                                        <a class="small" href="">Forgot Password?</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small" href="">Create an Account!</a>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <?php include ('js.php'); ?>

</body>

</html>