<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
    <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-procedures"></i>
    </div>
    <div class="sidebar-brand-text mx-3">RAWAT INAP</div>
</a>

<!-- Divider -->
<hr class="sidebar-divider my-0">

<!-- Nav Item - Dashboard -->
<li class="nav-item <?php if($page=="Dashboard"){ echo "active"; } ?>">
    <a class="nav-link" href="index.php">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
    Interface
</div>

<!-- Nav Item - Penyakit -->
<li class="nav-item <?php if($page=="Registrasi Rawat Inap"){ echo "active"; } ?>">
    <a class="nav-link" href="registrasirawatinap.php">
        <i class="fas fa-user-plus"></i>
        <span>Registrasi Rawat Inap</span></a>
</li>

<!-- Nav Item - Pasien -->
<li class="nav-item <?php if($page=="Pasien"){ echo "active"; } ?>">
    <a class="nav-link" href="pasien.php">
        <i class="fas fa-fw fa-user"></i>
        <span>Pasien</span></a>
</li>

<!-- Nav Item - Dokter -->
<li class="nav-item <?php if($page=="Dokter"){ echo "active"; } ?>">
    <a class="nav-link" href="dokter.php">
        <i class="fas fa-user-md"></i>
        <span>Dokter</span></a>
</li>

<!-- Nav Item - Data Obat -->
<li class="nav-item <?php if($page=="Data Obat"){ echo "active"; } ?>">
        <a class="nav-link" href="dataobat.php">
            <i class="fas fa-fw fa-capsules"></i>
            <span>Obat</span></a>
</li>

<!-- Nav Item - Penyakit -->
<li class="nav-item <?php if($page=="Pemeriksaan Rawat Inap"){ echo "active"; } ?>">
    <a class="nav-link" href="pemeriksaanrawatinap.php">
        <i class="fas fa-procedures"></i>
        <span>Pemeriksaan Rawat Inap</span></a>
</li>

<!-- Nav Item - Penyakit -->
<li class="nav-item <?php if($page=="Tindakan"){ echo "active"; } ?>">
    <a class="nav-link" href="tindakan.php">
        <i class="fas fa-fill-drip"></i>
        <span>Tindakan</span></a>
</li>

<!-- Nav Item - Penyakit -->
<li class="nav-item <?php if($page=="Penyakit"){ echo "active"; } ?>">
    <a class="nav-link" href="penyakit.php">
        <i class="fas fa-diagnoses"></i>
        <span>Penyakit</span></a>
</li>

<!-- Nav Item - Ruang -->
<li class="nav-item <?php if($page=="Ruang"){ echo "active"; } ?>">
    <a class="nav-link" href="ruang.php">
    <i class="fas fa-clinic-medical"></i>
        <span>Ruang</span></a>
</li>



<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>



</ul>
<!-- End of Sidebar -->