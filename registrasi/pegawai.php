<?php 
include '../koneksi.php';
$page="Pegawai"; 
if(isset($_POST['SimpanPegawai'])){
    $id_pegawai = $_POST['id_pegawai'];
    $nama_pegawai = $_POST['nama_pegawai'];
    mysqli_query($koneksi,"INSERT INTO pegawai VALUES(
        '$id_pegawai',
        '$nama_pegawai')");
    header("location:pegawai.php?pesan=input");
  }

  if(isset($_POST['EditPegawai'])){
    $id_pegawai = $_POST['id_pegawai'];
    $nama_pegawai = $_POST['nama_pegawai'];
    mysqli_query($koneksi,"UPDATE pegawai SET
        nama_pegawai = '$nama_pegawai'
        WHERE id_pegawai = '$id_pegawai'");
    header("location:pegawai.php?pesan=edit");
  }

  if(isset($_GET['id_pegawai'])){
    $id_pegawai = $_GET['id_pegawai'];
  
    mysqli_query($koneksi,"DELETE FROM pegawai WHERE id_pegawai='$id_pegawai'");
    header("location:pegawai.php?pesan=hapus");
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $page; ?> | Registrasi</title>

   <?php include ('css.php'); ?>

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php include ('sidebar.php'); ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include ('navbar.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800"><?= $page ?></h1>
                       
                    </div>
                    <!-- Content Row -->
                    <div class="row">

                        <!-- Content Column -->
                        <div class="col-lg-12 mb-4">
                        <?php
          if(isset($_GET['pesan'])){
            if($_GET['pesan'] == "input"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Ditambahkan</h4>
            </div>
            </marquee>
              ";
            }else if($_GET['pesan'] == "edit"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Diedit</h4>
            </div>
            </marquee>
              ";
            }else if($_GET['pesan'] == "hapus"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Dihapus</h4>
            </div>
            </marquee>
              ";
            }
          }
          ?>
                            <!-- Project Card Example -->
                            <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">
                                <a href="" class="btn btn-outline-primary btn-sm float-right" data-toggle="modal" data-target="#inputpegawai"><i class="fas fa fa-plus"></i> Tambah Pegawai</a>
                            </h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Pegawai</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Pegawai</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php
                                    $data = mysqli_query($koneksi,"SELECT * FROM pegawai");
                                    $no=1;
                                    while($d=mysqli_fetch_array($data)){
                                    ?>
                                        <tr>
                                            <td><?= $no++; ?></td>
                                            <td><?= $d['nama_pegawai']; ?></td>
                                            <td>
                                                <a href="" data-toggle="modal" data-target="#editpegawai<?php echo $no; ?>" class="btn btn-outline-primary"><i class="fas fa fa-edit"></i> Edit</a>
                                                <a href="" data-toggle="modal" data-target="#deletepegawai<?php echo $no; ?>" class="btn btn-outline-danger"><i class="fas fa fa-trash"></i> Delete</a>
                                            </td>
                                        </tr>

                                        <div class="modal fade" id="editpegawai<?= $no; ?>">
                                        <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title">Edit Pegawai</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <form method="post" action="#">
                                            <?php
                                                $id_pegawai = $d['id_pegawai'];
                                                $query = "SELECT * FROM pegawai WHERE id_pegawai='$id_pegawai'";
                                                $result = mysqli_query($koneksi,$query);
                                                while ($row = mysqli_fetch_assoc($result)){
                                            ?>
                                                <div class="card-body">
                                                <div class="form-group">
                                                    <label for="ID Pegawai">ID Pegawai</label>
                                                    <input type="text" class="form-control" id="id_pegawai" name="id_pegawai" value="<?= $row['id_pegawai']; ?>" readonly>
                                                </div>
                                                                        
                                                <div class="form-group">
                                                    <label for="Nama Pegawai">Nama Pegawai</label>
                                                    <input type="text" class="form-control" id="nama_pegawai" name="nama_pegawai" value="<?= $row['nama_pegawai']; ?>" required>
                                                </div>
                                                
                                                
                                                <div class="modal-footer justify-content-between">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary" name="EditPegawai">Simpan</button>
                                                </div>

                                                </div>
                                                <!-- /.card-body -->
                                                <?php } ?>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                    
                                    <div class="modal fade" id="deletepegawai<?php echo $no; ?>">
                                        <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title">Delete Pegawai</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <h4 align="center" >Apakah anda yakin ingin menghapus Pegawai dengan id <strong><?php echo $d['id_pegawai'];?></strong> dan dengan nama <strong><?php echo $d['nama_pegawai']; ?></strong> ?</h4>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                            <button id="nodelete" type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
                                            <a href="pegawai.php?id_pegawai=<?php echo $d['id_pegawai']; ?>" class="btn btn-primary">Delete</a>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->

                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                            
                    </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->
             
            <div class="modal fade" id="inputpegawai">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Pegawai</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
             <form method="post" action="#">
                <div class="card-body">
                  <div class="form-group">
                    <label for="ID Pegawai">ID Pegawai</label>
                    <input type="text" class="form-control" id="id_pegawai" placeholder="Ex : 1111" name="id_pegawai" required>
                  </div>
                                        
                  <div class="form-group">
                    <label for="Nama Pegawai">Nama Pegawai</label>
                    <input type="text" class="form-control" id="nama_pegawai" placeholder="Ex : Cindy" name="nama_pegawai" required>
                  </div>

                  
                  <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary" name="SimpanPegawai">Simpan</button>
                  </div>

                </div>
                <!-- /.card-body -->
                </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

            <?php include ('footer.php'); ?>

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    

   <?php include ('js.php'); ?>

</body>

</html>