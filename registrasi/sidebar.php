<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
    <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-hand-holding-medical"></i>
    </div>
    <div class="sidebar-brand-text mx-3">REGISTRASI</div>
</a>

<!-- Divider -->
<hr class="sidebar-divider my-0">

<!-- Nav Item - Dashboard -->
<li class="nav-item <?php if($page=="Dashboard"){ echo "active"; } ?>">
    <a class="nav-link" href="index.php">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
    Interface
</div>

<!-- Nav Item - Pasien -->
<li class="nav-item <?php if($page=="Pasien"){ echo "active"; } ?>">
    <a class="nav-link" href="pasien.php">
        <i class="fas fa-fw fa-user"></i>
        <span>Pasien</span></a>
</li>

<!-- Nav Item - registrasi rawat jalan -->
<li class="nav-item <?php if($page=="Rawat Jalan"){ echo "active"; } ?>">
    <a class="nav-link" href="registrasi_rawat_jalan.php">
        <i class="fas fa-stethoscope"></i>
        <span>Rawat Jalan</span></a>
</li>

<!-- Nav Item - registrasi rawat inap -->
<li class="nav-item <?php if($page=="Rawat Inap"){ echo "active"; } ?>">
    <a class="nav-link" href="registrasi_rawat_inap.php">
        <i class="fas fa-procedures"></i>
        <span>Rawat Inap</span></a>
</li>

<!-- Nav Item - Poli -->
<li class="nav-item <?php if($page=="Poli"){ echo "active"; } ?>">
    <a class="nav-link" href="poli.php">
        <i class="fas fa-medkit"></i>
        <span>Poli</span></a>
</li>

<!-- Nav Item - Pegawai -->
<li class="nav-item <?php if($page=="Pegawai"){ echo "active"; } ?>">
    <a class="nav-link" href="pegawai.php">
        <i class="fas fa-users"></i>
        <span>Pegawai</span></a>
</li>


<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>



</ul>
<!-- End of Sidebar -->