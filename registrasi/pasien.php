<?php 
include '../koneksi.php';
$page="Pasien"; 
if(isset($_POST['SimpanPasien'])){
    $nama_pasien = $_POST['nama_pasien'];
    $jk_pasien = $_POST['jk_pasien'];
    $tgl_lahir_pasien = $_POST['tgl_lahir_pasien'];
    $pekerjaan_pasien = $_POST['pekerjaan_pasien'];
    $alamat_pasien = $_POST['alamat_pasien'];
    $telepon_pasien = $_POST['telepon_pasien'];
    $wali_pasien = $_POST['wali_pasien'];
    $hub_wali_pasien = $_POST['hub_wali_pasien'];
    mysqli_query($koneksi,"INSERT INTO pasien VALUES(
        NULL,
        '$nama_pasien',
        '$jk_pasien',
        '$tgl_lahir_pasien',
        '$pekerjaan_pasien',
        '$alamat_pasien',
        '$telepon_pasien',
        '$wali_pasien',
        '$hub_wali_pasien')");
    header("location:pasien.php?pesan=input");
  }

  if(isset($_POST['EditPasien'])){
    $id_pasien = $_POST['id_pasien'];
    $nama_pasien = $_POST['nama_pasien'];
    $jk_pasien = $_POST['jk_pasien'];
    $tgl_lahir_pasien = $_POST['tgl_lahir_pasien'];
    $pekerjaan_pasien = $_POST['pekerjaan_pasien'];
    $alamat_pasien = $_POST['alamat_pasien'];
    $telepon_pasien = $_POST['telepon_pasien'];
    $wali_pasien = $_POST['wali_pasien'];
    $hub_wali_pasien = $_POST['hub_wali_pasien'];
    mysqli_query($koneksi,"UPDATE pasien SET
        nama_pasien = '$nama_pasien',
        jk_pasien = '$jk_pasien',
        tgl_lahir_pasien = '$tgl_lahir_pasien',
        pekerjaan_pasien = '$pekerjaan_pasien',
        alamat_pasien = '$alamat_pasien',
        telepon_pasien = '$telepon_pasien',
        wali_pasien = '$wali_pasien',
        hub_wali_pasien = '$hub_wali_pasien'
        WHERE id_pasien = '$id_pasien'");
    header("location:pasien.php?pesan=edit");
  }

  if(isset($_GET['id_pasien'])){
    $id_pasien = $_GET['id_pasien'];
  
    mysqli_query($koneksi,"DELETE FROM pasien WHERE id_pasien='$id_pasien'");
    header("location:pasien.php?pesan=hapus");
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $page; ?> | Registrasi</title>

   <?php include ('css.php'); ?>

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php include ('sidebar.php'); ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include ('navbar.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800"><?= $page ?></h1>
                       
                    </div>
                    <!-- Content Row -->
                    <div class="row">

                        <!-- Content Column -->
                        <div class="col-lg-12 mb-4">
                        <?php
          if(isset($_GET['pesan'])){
            if($_GET['pesan'] == "input"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Ditambahkan</h4>
            </div>
            </marquee>
              ";
            }else if($_GET['pesan'] == "edit"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Diedit</h4>
            </div>
            </marquee>
              ";
            }else if($_GET['pesan'] == "hapus"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Dihapus</h4>
            </div>
            </marquee>
              ";
            }
          }
          ?>
                            <!-- Project Card Example -->
                            <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">
                                <a href="" class="btn btn-outline-primary btn-sm float-right" data-toggle="modal" data-target="#inputpasien"><i class="fas fa fa-plus"></i> Tambah Pasien</a>
                            </h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Pasien</th>
                                            <th>Jenis Kelamin Pasien</th>
                                            <th>Tgl Lahir Pasien</th>
                                            <th>Pekerjaan Pasien</th>
                                            <th>Alamat Pasien</th>
                                            <th>Telepon Pasien</th>
                                            <th>Wali Pasien</th>
                                            <th>Hubungan Wali Pasien</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $data = mysqli_query($koneksi,"SELECT * FROM pasien");
                                    $no=1;
                                    while($d=mysqli_fetch_array($data)){
                                    ?>
                                        <tr>
                                            <td><?= $no++; ?></td>
                                            <td><?= $d['nama_pasien']; ?></td>
                                            <td><?= $d['jk_pasien']; ?></td>
                                            <td><?= $d['tgl_lahir_pasien']; ?></td>
                                            <td><?= $d['pekerjaan_pasien']; ?></td>
                                            <td><?= $d['alamat_pasien']; ?></td>
                                            <td><?= $d['telepon_pasien']; ?></td>
                                            <td><?= $d['wali_pasien']; ?></td>
                                            <td><?= $d['hub_wali_pasien']; ?></td>
                                            <td>
                                                <a href="" data-toggle="modal" data-target="#editpasien<?php echo $no; ?>" class="btn btn-outline-primary"><i class="fas fa fa-edit"></i> Edit</a>
                                                <a href="" data-toggle="modal" data-target="#deletepasien<?php echo $no; ?>" class="btn btn-outline-danger"><i class="fas fa fa-trash"></i> Delete</a>
                                            </td>
                                        </tr>

                                        <div class="modal fade" id="editpasien<?= $no; ?>">
                                        <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title">Edit Pasien</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <form method="post" action="#">
                                            <?php
                                                $id_pasien = $d['id_pasien'];
                                                $query = "SELECT * FROM pasien WHERE id_pasien='$id_pasien'";
                                                $result = mysqli_query($koneksi,$query);
                                                while ($row = mysqli_fetch_assoc($result)){
                                            ?>
                                                <div class="card-body">
                                                <div class="form-group">
                                                    <label for="ID Pasien">ID Pasien</label>
                                                    <input type="text" class="form-control" id="id_pasien" name="id_pasien" value="<?= $row['id_pasien']; ?>" readonly>
                                                </div>
                                                                        
                                                <div class="form-group">
                                                    <label for="Nama Pasien">Nama Pasien</label>
                                                    <input type="text" class="form-control" id="nama_pasien" name="nama_pasien" value="<?= $row['nama_pasien']; ?>" required>
                                                </div>

                                                <div class="form-group">
                                                    <label for="Jenis Kelamin Pasien">Jenis Kelamin Pasien</label>
                                                    <select name="jk_pasien" id="jk_pasien" class="form-control">
                                                        <option value="Laki-laki" <?php if($row['jk_pasien']=="Laki-laki"){ echo "selected"; } ?> >Laki-laki</option>
                                                        <option value="Perempuan" <?php if($row['jk_pasien']=="perempuan"){ echo "selected"; } ?> >Perempuan</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="Tanggal Lahir pasien">Tanggal Lahir pasien</label>
                                                    <input type="date" name="tgl_lahir_pasien" id="tgl_lahir_pasien" class="form-control" value="<?= $row['tgl_lahir_pasien']; ?>">
                                                </div>

                                                <div class="form-group">
                                                    <label for="Pekerjaan Pasien">Pekerjaan Pasien</label>
                                                    <input type="text" name="pekerjaan_pasien" id="pekerjaan_pasien" class="form-control" value="<?= $row['pekerjaan_pasien']; ?>" required>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="Alamat Pasien">Alamat Pasien</label>
                                                    <input type="text" name="alamat_pasien" id="alamat_pasien" class="form-control" value="<?= $row['alamat_pasien']; ?>" required>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="Telepeon Pasien">Telepeon Pasien</label>
                                                    <input type="number" name="telepon_pasien" id="telepon_pasien" class="form-control" value="<?= $row['telepon_pasien']; ?>" required>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="Wali Pasien">Wali Pasien</label>
                                                    <input type="text" name="wali_pasien" id="wali_pasien" class="form-control" value="<?= $row['wali_pasien']; ?>" required>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="Hubungan Wali Pasien">Hubungan Wali Pasien</label>
                                                    <input type="text" name="hub_wali_pasien" id="hub_wali_pasien" class="form-control" value="<?= $row['hub_wali_pasien']; ?>" required>
                                                </div>
                                                
                                                
                                                <div class="modal-footer justify-content-between">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary" name="EditPasien">Simpan</button>
                                                </div>

                                                </div>
                                                <!-- /.card-body -->
                                                <?php } ?>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                    
                                    <div class="modal fade" id="deletepasien<?php echo $no; ?>">
                                        <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title">Delete Pasien</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <h4 align="center" >Apakah anda yakin ingin menghapus pasien dengan id <strong><?php echo $d['id_pasien'];?></strong> dan dengan nama <strong><?php echo $d['nama_pasien']; ?></strong> ?</h4>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                            <button id="nodelete" type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
                                            <a href="pasien.php?id_pasien=<?php echo $d['id_pasien']; ?>" class="btn btn-primary">Delete</a>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->

                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                            
                    </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->
             
            <div class="modal fade" id="inputpasien">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Pasien</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
             <form method="post" action="#">
                <div class="card-body">            
                  <div class="form-group">
                    <label for="Nama Pasien">Nama Pasien</label>
                    <input type="text" class="form-control" id="nama_pasien" placeholder="Ex : Berto" name="nama_pasien" required>
                  </div>
                  
                  <div class="form-group">
                    <label for="Jenis Kelamin pasien">Jenis Kelamin pasien</label>
                    <select name="jk_pasien" id="jk_pasien" class="form-control">
                        <option value="">-- Pilih Jenis kelamin pasien --</option>
                        <option value="Laki-laki">Laki-laki</option>
                        <option value="Perempuan">Perempuan</option>
                    </select>
                  </div> 

                  <div class="form-group">
                    <label for="Tanggal Lahir pasien">Tanggal Lahir pasien</label>
                    <input type="date" name="tgl_lahir_pasien" id="tgl_lahir_pasien" class="form-control">
                  </div>

                  <div class="form-group">
                    <label for="Pekerjaan Pasien">Pekerjaan Pasien</label>
                    <input type="text" name="pekerjaan_pasien" id="pekerjaan_pasien" placeholder="Ex : PNS" class="form-control" required>
                  </div>
                  
                  <div class="form-group">
                    <label for="Alamat Pasien">Alamat Pasien</label>
                    <input type="text" name="alamat_pasien" id="alamat_pasien" placeholder="Ex : Kepuh" class="form-control" required>
                  </div>                     

                  <div class="form-group">
                    <label for="Telepon Pasien">Telepon Pasien</label>
                    <input id="telepon_pasien" type="number" name="telepon_pasien" class="form-control" placeholder="Ex : No Telp." required>
                  </div>
                  
                  <div class="form-group">
                    <label for="Wali Pasien">Wali Pasien</label>
                    <input type="text" class="form-control" id="wali_pasien" placeholder="Ex : Berta" name="wali_pasien" required>
                  </div>
                  
                  <div class="form-group">
                    <label for="Hubungan Wali Pasien">Hubungan Wali Pasien</label>
                    <input type="text" class="form-control" id="hub_wali_pasien" placeholder="Ex : Istri" name="hub_wali_pasien" required>
                  </div>
                  
                  <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary" name="SimpanPasien">Simpan</button>
                  </div>

                </div>
                <!-- /.card-body -->
                </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

            <?php include ('footer.php'); ?>

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    

   <?php include ('js.php'); ?>

</body>

</html>