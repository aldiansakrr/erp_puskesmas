<?php 
include '../koneksi.php';
include 'config.php';
$page="Detail Resep Rawat Jalan ";

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $page; ?> | Apotek</title>

   <?php include ('css.php'); ?>

</head>
<?php
  session_start();
    if($_SESSION['id_pegawai']==""){

    header("location:login.php?pesan=belumlogin");
  }
  ?>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php include ('sidebar.php'); ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include ('navbar.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800"><?= $page ?></h1>
                       
                    </div>
                    <!-- Content Row -->
                    <div class="row">

                        <!-- Content Column -->
                        <div class="col-lg-12 mb-4">
        
                            <!-- Project Card Example -->
                            <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">
                                Detail Resep <strong> <?= $_GET['id_resep_rawat_jalan']; ?> </strong>
                            </h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered"  width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ID Resep Rawat Inap</th>
                                            <th>Obat</th>
                                            <th>Jumlah</th>
                                            <th>Harga</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>ID Resep Rawat Inap</th>
                                            <th>Obat</th>
                                            <th>Jumlah</th>
                                            <th>Harga</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php
                                    $id_resep_rawat_jalan = $_GET['id_resep_rawat_jalan'];
                                    $data = mysqli_query($koneksi,"SELECT * FROM detail_resep_rawat_jalan JOIN resep_rawat_jalan ON 
                                    detail_resep_rawat_jalan.id_resep_rawat_jalan = resep_rawat_jalan.id_resep_rawat_jalan 
                                    JOIN obat ON detail_resep_rawat_jalan.id_obat = obat.id_obat WHERE detail_resep_rawat_jalan.id_resep_rawat_jalan = '$id_resep_rawat_jalan'");
                                    $no=1;
                                    while($d=mysqli_fetch_array($data)){
                                    ?>
                                        <tr>
                                            <td><?= $no++; ?></td>
                                            <td><?= $d['id_resep_rawat_jalan']; ?></td>
                                            <td><?= $d['nama_obat']; ?></td>
                                            <td><?= $d['jumlah']; ?></td>
                                            <td>Rp. <?= number_format($d['harga_obat']) ?></td>
                                            
                                        </tr>                                                         
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <a href="v_resep_rawatjalan.php" class="btn btn-outline-primary">Back</a>
                            </div>
                        </div>
                    </div>

                            
                    </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <?php include ('footer.php'); ?>

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    

   <?php include ('js.php'); ?>

</body>

</html>