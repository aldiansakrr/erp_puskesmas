<?php 
include '../koneksi.php';
$page="Rawat Jalan"; 
if(isset($_POST['SimpanRawatJalan'])){
    $id_pasien = $_POST['id_pasien'];
    $tgl_reg_rawat_jalan = $_POST['tgl_reg_rawat_jalan'];
    $keterangan = $_POST['keterangan'];
    mysqli_query($koneksi,"INSERT INTO registrasi_rawat_jalan  VALUES(NULL, '$tgl_reg_rawat_jalan', '$id_pasien', '$keterangan')");
    header("location:registrasi_rawat_jalan.php?pesan=input");
  }

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $page; ?> | Registrasi</title>

   <?php include ('css.php'); ?>

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php include ('sidebar.php'); ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include ('navbar.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800"><?= $page ?></h1>
                       
                    </div>
                    <!-- Content Row -->
                    <div class="row">

                        <!-- Content Column -->
                        <div class="col-lg-12 mb-4">
                        <?php
          if(isset($_GET['pesan'])){
            if($_GET['pesan'] == "input"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Ditambahkan</h4>
            </div>
            </marquee>
              ";
            }else if($_GET['pesan'] == "edit"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Diedit</h4>
            </div>
            </marquee>
              ";
            }else if($_GET['pesan'] == "hapus"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Dihapus</h4>
            </div>
            </marquee>
              ";
            }
          }
          ?>
                            <!-- Project Card Example -->
                            <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">
                                <a href="" class="btn btn-outline-primary btn-sm float-right" data-toggle="modal" data-target="#inputpemeriksaan"><i class="fas fa fa-plus"></i> Tambah Pemeriksaan</a>
                            </h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Tanggal</th>
                                            <th>Nama Pasien</th>
                                            <th>Keterangan</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $data = mysqli_query($koneksi,"SELECT * FROM registrasi_rawat_jalan join pasien on registrasi_rawat_jalan.id_pasien=pasien.id_pasien");
                                    $no=1;
                                    while($d=mysqli_fetch_array($data)){
                                    ?>
                                        <tr>
                                            <td><?= $no++; ?></td>
                                            <td><?= $d['tgl_reg_rawat_jalan']; ?></td>
                                            <td><?= $d['nama_pasien']; ?></td>
                                            <td><?= $d['keterangan']; ?></td>
                                            <td>
                                                <a target="_blank" href="kartu_rawatjalan.php?no_reg_rawat_jalan=<?= $d['no_reg_rawat_jalan']; ?>" class="btn btn-outline-success"><i class="fas fa fa-print"></i> Cetak</a>
                                            </td>
                                        </tr>

                                    
                                    
                                   

                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
        
                    </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->
             
            <div class="modal fade" id="inputpemeriksaan">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Rawat Jalan</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
             <form method="post" action="#">
                <div class="card-body">
                    
                <div class="form-group">
                    <label for="Nama Pasien">Tanggal Regis</label>
                    <input type="date" name="tgl_reg_rawat_jalan" class="form-control" value="<?= date('Y-m-d'); ?>">
                </div>

                  <div class="form-group">
                    <label for="Nama Pasien">Nama Pasien</label>
                    <select name="id_pasien" class="form-control">
                        <?php
                        $pasiens = mysqli_query($koneksi,"SELECT * FROM pasien");
                        while($pasien = mysqli_fetch_array($pasiens)){
                        ?>
                        <option value="<?php echo $pasien['id_pasien']; ?>"> <?php echo $pasien['nama_pasien']; ?> </option>
                        <?php } ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="Nama Pasien">Keterangan</label>
                    <input type="text" name="keterangan" class="form-control">
                </div>
                  
                  <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary" name="SimpanRawatJalan">Simpan</button>
                  </div>

                </div>
                <!-- /.card-body -->
                </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

            <?php include ('footer.php'); ?>

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

   <?php include ('js.php'); ?>

</body>

</html>