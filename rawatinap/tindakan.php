<?php 
include '../koneksi.php';
$page="Tindakan"; 
if(isset($_POST['SimpanTindakan'])){
    $id_tindakan = $_POST['id_tindakan'];
    $nama_tindakan = $_POST['nama_tindakan'];
    $biaya_tindakan = $_POST['biaya_tindakan'];
    mysqli_query($koneksi,"INSERT INTO tindakan VALUES(
        '$id_tindakan',
        '$nama_tindakan',
        '$biaya_tindakan')");
    header("location:tindakan.php?pesan=input");
  }

  if(isset($_POST['EditTindakan'])){
    $id_tindakan = $_POST['id_tindakan'];
    $nama_tindakan = $_POST['nama_tindakan'];
    $biaya_tindakan = $_POST['biaya_tindakan'];
    mysqli_query($koneksi,"UPDATE tindakan SET
        nama_tindakan = '$nama_tindakan',
        biaya_tindakan = '$biaya_tindakan'
        WHERE id_tindakan = '$id_tindakan'");
    header("location:tindakan.php?pesan=edit");
  }

  if(isset($_GET['id_tindakan'])){
    $id_tindakan = $_GET['id_tindakan'];
  
    mysqli_query($koneksi,"DELETE FROM tindakan WHERE id_tindakan='$id_tindakan'");
    header("location:tindakan.php?pesan=hapus");
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $page; ?> | Rawat Inap</title>

   <?php include ('css.php'); ?>

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php include ('sidebar.php'); ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include ('navbar.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800"><?= $page ?></h1>
                       
                    </div>
                    <!-- Content Row -->
                    <div class="row">

                        <!-- Content Column -->
                        <div class="col-lg-12 mb-4">
                        <?php
          if(isset($_GET['pesan'])){
            if($_GET['pesan'] == "input"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Ditambahkan</h4>
            </div>
            </marquee>
              ";
            }else if($_GET['pesan'] == "edit"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Diedit</h4>
            </div>
            </marquee>
              ";
            }else if($_GET['pesan'] == "hapus"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Dihapus</h4>
            </div>
            </marquee>
              ";
            }
          }
          ?>
                            <!-- Project Card Example -->
                            <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">
                                <a href="" class="btn btn-outline-primary btn-sm float-right" data-toggle="modal" data-target="#inputtindakan"><i class="fas fa fa-plus"></i> Tambah Tindakan</a>
                            </h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Tindakan</th>
                                            <th>Biaya Tindakan</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Tindakan</th>
                                            <th>Biaya Tindakan</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php
                                    $data = mysqli_query($koneksi,"SELECT * FROM tindakan");
                                    $no=1;
                                    while($d=mysqli_fetch_array($data)){
                                    ?>
                                        <tr>
                                            <td><?= $no++; ?></td>
                                            <td><?= $d['nama_tindakan']; ?></td>
                                            <td>Rp. <?= number_format($d['biaya_tindakan']); ?></td>
                                            <td>
                                                <a href="" data-toggle="modal" data-target="#edittindakan<?php echo $no; ?>" class="btn btn-outline-primary"><i class="fas fa fa-edit"></i> Edit</a>
                                                <a href="" data-toggle="modal" data-target="#deletetindakan<?php echo $no; ?>" class="btn btn-outline-danger"><i class="fas fa fa-trash"></i> Delete</a>
                                            </td>
                                        </tr>

                                        <div class="modal fade" id="edittindakan<?= $no; ?>">
                                        <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title">Edit Tindakan</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <form method="post" action="#">
                                            <?php
                                                $id_tindakan = $d['id_tindakan'];
                                                $query = "SELECT * FROM tindakan WHERE id_tindakan='$id_tindakan'";
                                                $result = mysqli_query($koneksi,$query);
                                                while ($row = mysqli_fetch_assoc($result)){
                                            ?>
                                                <div class="card-body">
                                                <div class="form-group">
                                                    <label for="ID Tindakan">ID Tindakan</label>
                                                    <input type="text" class="form-control" id="id_tindakan" name="id_tindakan" value="<?= $row['id_tindakan']; ?>" readonly>
                                                </div>
                                                                        
                                                <div class="form-group">
                                                    <label for="Nama Tindakan">Nama Tindakan</label>
                                                    <input type="text" class="form-control" id="nama_tindakan" name="nama_tindakan" value="<?= $row['nama_tindakan']; ?>" required>
                                                </div>

                                                <div class="form-group">
                                                    <label for="Biaya Tindakan">Biaya Tindakan</label>
                                                    <input type="text" class="form-control" id="biaya_tindakan" name="biaya_tindakan" value="<?= $row['biaya_tindakan']; ?>" required>
                                                </div>
                                                
                                                
                                                <div class="modal-footer justify-content-between">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary" name="EditTindakan">Simpan</button>
                                                </div>

                                                </div>
                                                <!-- /.card-body -->
                                                <?php } ?>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                    
                                    <div class="modal fade" id="deletetindakan<?php echo $no; ?>">
                                        <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title">Delete Tindakan</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <h4 align="center" >Apakah anda yakin ingin menghapus tindakan dengan id <strong><?php echo $d['id_tindakan'];?></strong> dan dengan nama <strong><?php echo $d['nama_tindakan']; ?></strong> ?</h4>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                            <button id="nodelete" type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
                                            <a href="tindakan.php?id_tindakan=<?php echo $d['id_tindakan']; ?>" class="btn btn-primary">Delete</a>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->

                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                            
                    </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->
             
            <div class="modal fade" id="inputtindakan">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Tindakan</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
             <form method="post" action="#">
                <div class="card-body">
                  <div class="form-group">
                    <label for="ID Tindakan">ID Tindakan</label>
                    <input type="text" class="form-control" id="id_tindakan" placeholder="Ex : Z00 " name="id_tindakan" required>
                  </div>
                                        
                  <div class="form-group">
                    <label for="Nama Tindakan">Nama Tindakan</label>
                    <input type="text" class="form-control" id="nama_tindakan" placeholder="Ex : Pemeriksaan" name="nama_tindakan" required>
                  </div>

                  <div class="form-group">
                    <label for="Biaya Tindakan">Biaya Tindakan</label>
                    <input type="text" class="form-control" id="biaya_tindakan" placeholder="Ex : Pemeriksaan" name="biaya_tindakan" required>
                  </div>

                  
                  <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary" name="SimpanTindakan">Simpan</button>
                  </div>

                </div>
                <!-- /.card-body -->
                </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

            <?php include ('footer.php'); ?>

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    

   <?php include ('js.php'); ?>

</body>

</html>