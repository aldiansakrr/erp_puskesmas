<?php 
include '../koneksi.php';
$page="Registrasi Rawat Inap"; 
if(isset($_POST['SimpanRawatInap'])){
    $no_reg_rawat_inap = $_POST['no_reg_rawat_inap'];
    $id_dokter = $_POST['id_dokter'];
    $id_pasien = $_POST['id_pasien'];
    $id_resep = $_POST['id_resep'];
    $id_ruang = $_POST['id_ruang'];
    $tanggal = date('Y-m-d');
    $total_biaya = 0;
    mysqli_query($koneksi,"INSERT INTO pemeriksaan_rawat_inap  VALUES(NULL, '$no_reg_rawat_inap', '$id_dokter', '$id_pasien','$id_resep','$id_ruang','$total_biaya')");
    mysqli_query($koneksi,"INSERT INTO resep_rawat_inap VALUES('$id_resep','$tanggal','$id_dokter','$total_biaya','BELUM DISERAHKAN')");
    header("location:pemeriksaanrawatinap.php?pesan=input");
  }

  if(isset($_POST['EditRawatInap'])){
    $no_periksa_rawat_inap = $_POST['no_periksa_rawat_inap'];
    $pasien_id = $_POST['pasien_id'];
    mysqli_query($koneksi,"UPDATE pasien SET pasien_id = '$pasien_id', WHERE no_periksa_rawat_inap = '$no_periksa_rawat_inap'");
    header("location:registrasi_rawat_inap.php?pesan=edit");
  }

  if(isset($_GET['no_periksa_rawat_inap'])){
    $no_periksa_rawat_inap = $_GET['no_periksa_rawat_inap'];
  
    mysqli_query($koneksi,"DELETE FROM pasien WHERE no_periksa_rawat_inap='$no_periksa_rawat_inap'");
    header("location:registrasi_rawat_inap.php?pesan=hapus");
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $page; ?> | Rawat Inap</title>

   <?php include ('css.php'); ?>

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php include ('sidebar.php'); ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include ('navbar.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800"><?= $page ?></h1>
                       
                    </div>
                    <!-- Content Row -->
                    <div class="row">

                        <!-- Content Column -->
                        <div class="col-lg-12 mb-4">
                        <?php
          if(isset($_GET['pesan'])){
            if($_GET['pesan'] == "input"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Ditambahkan</h4>
            </div>
            </marquee>
              ";
            }else if($_GET['pesan'] == "edit"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Diedit</h4>
            </div>
            </marquee>
              ";
            }else if($_GET['pesan'] == "hapus"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Dihapus</h4>
            </div>
            </marquee>
              ";
            }
          }
          ?>
                            <!-- Project Card Example -->
                            <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Tanggal</th>
                                            <th>Nama Pasien</th>
                                            <th>Keterangan</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                        <th>#</th>
                                            <th>Tanggal</th>
                                            <th>Nama Pasien</th>
                                            <th>Keterangan</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php
                                    $data = mysqli_query($koneksi,"SELECT * FROM registrasi_rawat_inap join pasien on registrasi_rawat_inap.id_pasien=pasien.id_pasien");
                                    $no=1;
                                    while($d=mysqli_fetch_array($data)){
                                    ?>
                                        <tr>
                                            <td><?= $no++; ?></td>
                                            <td><?= $d['tgl_reg_rawat_inap']; ?></td>
                                            <td><?= $d['nama_pasien']; ?></td>
                                            <td><?= $d['keterangan']; ?></td>
                                            <td>
                                                <a href="" class="btn btn-outline-success" data-toggle="modal" data-target="#proses<?= $no; ?>"><i class="fas fa fa-edit"></i> Proses</a>
                                            </td>
                                        </tr>

                                        <div class="modal fade" id="proses<?= $no; ?>">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Tambah Pemeriksaan Rawat Inap</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                    <form method="post" action="#">
                                      <?php 
                                      $no_reg_rawat_inap = $d['no_reg_rawat_inap'];
                                      $regist = mysqli_query($koneksi,"SELECT * FROM registrasi_rawat_inap JOIN pasien ON registrasi_rawat_inap.id_pasien = pasien.id_pasien WHERE no_reg_rawat_inap = '$no_reg_rawat_inap'");
                                      while ($row = mysqli_fetch_array($regist)){
                                      ?>
                                        <div class="card-body">
                                          <div class="form-group">
                                            <label for="">No Registrasi</label>
                                            <input type="number" class="form-control" id="no_reg_rawat_inap" value="<?= $row['no_reg_rawat_inap']; ?>" name="no_reg_rawat_inap" readonly>
                                          </div>
                                                                
                                          <div class="form-group">
                                            <label for="">Dokter</label>
                                            <select name="id_dokter" class="form-control">
                                              <?php 
                                              $dokters = mysqli_query($koneksi,"SELECT * FROM dokter");
                                              while($dokter = mysqli_fetch_array($dokters)){
                                              ?>
                                              <option value="<?= $dokter['id_dokter']; ?>"><?= $dokter['nama_dokter']; ?></option>
                                              <?php } ?>
                                            </select>
                                          </div>

                                          <div class="form-group">
                                            <label for="">Pasien</label>
                                            <input type="text" name="id_pasien" class="form-control" value="<?= $row['id_pasien']; ?>" readonly>
                                          </div>
                                          
                                          <div class="form-group">
                                            <label for="">ID Resep</label>
                                            <input type="number" class="form-control" id="id_resep" placeholder="Ex : 123" name="id_resep" required>
                                          </div>

                                          <div class="form-group">
                                            <label for="">Ruang</label>
                                          <select name="id_ruang" class="form-control">
                                            <?php 
                                            $ruangs = mysqli_query($koneksi,"SELECT * FROM ruang");
                                            while($ruang = mysqli_fetch_array($ruangs)){
                                            ?>
                                            <option value="<?= $ruang['id_ruang']; ?>"><?= $ruang['nama_ruang']; ?></option>
                                            <?php } ?>
                                          </select> 
                                          </div>
                                          
                                          <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          <button type="submit" class="btn btn-primary" name="SimpanRawatInap">Simpan</button>
                                          </div>
                                        <?php } ?>
                                        </div>
                                        <!-- /.card-body -->
                                        </form>
                                    </div>
                                  </div>
                                  <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                              </div>
                              <!-- /.modal -->
                                    
                                   

                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
        
                    </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->
                <!-- /.card-body -->
                </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

            <?php include ('footer.php'); ?>

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

   <?php include ('js.php'); ?>

</body>

</html>