<?php 
include '../koneksi.php';
include 'config.php';
$page="Resep Rawat Inap";



if(isset($_POST['addToCart'])){

    $id_obat = $_POST["id_obat"];
    $id_pegawai = $_POST["id_pegawai"];
    $banyak = $_POST["banyak"];
    $obat = mysqli_fetch_array(mysqli_query($koneksi,"SELECT * FROM obat WHERE id_obat = '$id_obat'"));
    $cekobat = mysqli_fetch_array(mysqli_query($koneksi,"SELECT * FROM carts WHERE id_obat = '$id_obat'"));

    if($cekobat){

    }
    

    $total = $banyak * $obat["harga_obat"];

    $queryCart = "INSERT INTO carts
                VALUES
                (NULL, '$id_pegawai', '$id_obat', '$banyak', '$total')
            ";
    
    $resultt = mysqli_query($koneksi, $queryCart);

    $stockObat=  $obat["stok_obat"] - $banyak;

    $queryObat = "UPDATE obat SET
                        stok_obat = '$stockObat'
                    WHERE id_obat = $id_obat
                    ";
    $result = mysqli_query($koneksi, $queryObat);
    
}

if (isset($_POST["deleteCart"])) {
  if (deleteProductAtCart($_POST) > 0) {
    echo "<script>
            alert('Product berhasil di hapus dari cart Anda');
            document.location.href = 'resep_rawatinap.php';
          </script>";
  }
}

if(isset($_POST['SubmitResep'])) {
  if(submitResep($_POST) > 0){
    echo "<script>
            alert('Resep berhasil ditambahkan');
            document.location.href = 'resep_rawatinap.php';
          </script>";
  }else {
    echo "<script>
            alert('Resep gagal ditambahkan');
            document.location.href = 'resep_rawatinap.php';
          </script>";
  }

  
}

  
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $page; ?> | Apotek</title>

   <?php include ('css.php'); ?>

</head>
<?php
  session_start();
    if($_SESSION['id_pegawai']==""){

    header("location:login.php?pesan=belumlogin");
  }
  ?>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php include ('sidebar.php'); ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include ('navbar.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800"><?= $page ?></h1>
                       
                    </div>
                    <!-- Content Row -->
                    <div class="row">

                        <!-- Content Column -->
                        <div class="col-lg-12 mb-4">
                            <!-- Project Card Example -->
                            <div class="card shadow mb-4">
                        
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Obat</th>
                                            <th>Harga Obat</th>
                                            <th>Jumlah</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Obat</th>
                                            <th>Harga Obat</th>
                                            <th>Jumlah</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php
                                    $data = mysqli_query($koneksi,"SELECT * FROM obat");
                                    $no=1;
                                    while($d=mysqli_fetch_array($data)){
                                    ?>
                                        <tr>
                                            <td><?= $no++; ?></td>
                                            <td><?= $d['nama_obat']; ?></td>
                                            <td><?= $d['harga_obat']; ?></td>
                                            <form action="" method="POST">
                                              <td>
                                                <input type="number" name="banyak" value="1" class="form-control w-50">
                                              </td>
                                            <td>
                                            <input type="hidden" name="id_pegawai" value="<?= $_SESSION['id_pegawai']; ?>">
                                                <input type="hidden" name="id_obat" value="<?= $d['id_obat']; ?>">
                                            <button type="submit" name="addToCart" class="btn btn-outline-success px-4  btn-block mb-3 py-2">
                                                
                                                    Add to cart
                                                  </button>
                                                </td>
                                                
                                                </form>
                                            
                                        </tr>


                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                            
                    </div>

                    <div class="col-lg-12 mb-4">
                      <div class="card shadow mb-4">
                        <div class="card-body">
                          <div class="table-responsive">
                          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Nama Obat</th>
                                            <th>Banyak</th>
                                            <th>Total</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $id_pegawai = $_SESSION['id_pegawai'];
                                    $data2 = mysqli_query($koneksi,"SELECT * FROM carts JOIN obat ON carts.id_obat = obat.id_obat WHERE id_pegawai = '$id_pegawai' ");
                                    $bayar=0;
                                    while($d2=mysqli_fetch_array($data2)){
                                    $bayar+=$d2['total'];
                                    ?>
                                    <tr>
                                      <td><?= $d2['nama_obat']; ?></td>
                                      <td><?= $d2['banyak']; ?></td>
                                      <td><?= $d2['total']; ?></td>
                                      <td style="width:100px">
                                      <form action="" method="POST">
                                      <input type="hidden" name="id_carts" value="<?= $d2["id_carts"]; ?>">
                                      <button type="submit" onclick="return confirm('Yakin Ingin Menghapus obat ini  ?')" name="deleteCart" class="btn btn-outline-danger">Remove</button>
                                    </form>
                                      </td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                          </table>
                          </div>
                          <hr>
                          <form action="" method="post">
                            <div class="form-group">
                              <label for="Tanggal Resep">Tanggal Resep</label>
                              <input type="date" name="tanggal" class="form-control" value="<?= date('Y-m-d'); ?>" readonly>
                            </div>
                            <div class="form-group">
                              <label for="id_dokter">Dokter</label>
                              <select name="id_dokter" class="form-control">
                                <?php 
                                $dokters = mysqli_query($koneksi,"SELECT * FROM dokter"); 
                                while($dokter = mysqli_fetch_array($dokters)) {
                                ?>
                                <option value="<?= $dokter['id_dokter']; ?>"><?= $dokter['nama_dokter']; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="id_poli">Poli</label>
                              <select name="id_poli" class="form-control">
                                <?php 
                                $polis = mysqli_query($koneksi,"SELECT * FROM poli"); 
                                while($poli = mysqli_fetch_array($polis)) {
                                ?>
                                <option value="<?= $poli['id_poli']; ?>"><?= $poli['nama_poli']; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="id_pasien">Pasien</label>
                              <select name="id_pasien" class="form-control">
                                <?php
                                $pasiens = mysqli_query($koneksi, "SELECT * FROM pasien"); 
                                while($pasien = mysqli_fetch_array($pasiens)) {
                                ?>
                                <option value="<?= $pasien['id_pasien']; ?>"><?= $pasien['nama_pasien']; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="total_bayar">Total Bayar</label>
                              <input type="number" name="total_bayar" class="form-control" value="<?= $bayar ?>" readonly>
                            </div>
                            <div class="form-group">
                              <label for="">Resep</label>
                              <select name="resep" class="form-control">
                                <option value="1">Rawat Inap</option>
                                <option value="2">Rawat Jalan</option>
                              </select>
                            </div>
                            <div class="form-group">
                              <input type="hidden" name="id_pegawai" class="form-control" value="<?= $_SESSION['id_pegawai']; ?>">
                              <button type="submit" class="btn btn-outline-success float-right" name="SubmitResep">Submit</button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->
             

            <?php include ('footer.php'); ?>

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    

   <?php include ('js.php'); ?>

</body>

</html>