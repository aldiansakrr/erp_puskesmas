-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 11, 2023 at 09:12 PM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `puskesmas`
--

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

DROP TABLE IF EXISTS `carts`;
CREATE TABLE IF NOT EXISTS `carts` (
  `id_carts` int(11) NOT NULL AUTO_INCREMENT,
  `id_pegawai` int(11) NOT NULL,
  `id_obat` int(11) NOT NULL,
  `banyak` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  PRIMARY KEY (`id_carts`)
) ENGINE=MyISAM AUTO_INCREMENT=111 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_pemeriksaan_rawatinap`
--

DROP TABLE IF EXISTS `detail_pemeriksaan_rawatinap`;
CREATE TABLE IF NOT EXISTS `detail_pemeriksaan_rawatinap` (
  `no_periksa_rawat_inap` int(11) NOT NULL,
  `id_penyakit` int(11) NOT NULL,
  `id_tindakan` int(11) NOT NULL,
  `biaya_tindakan` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_pemeriksaan_rawatjalan`
--

DROP TABLE IF EXISTS `detail_pemeriksaan_rawatjalan`;
CREATE TABLE IF NOT EXISTS `detail_pemeriksaan_rawatjalan` (
  `no_periksa_rawat_jalan` int(11) NOT NULL,
  `id_penyakit` int(11) NOT NULL,
  `id_tindakan` int(11) NOT NULL,
  `biaya_tindakan` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pemeriksaan_rawatjalan`
--

INSERT INTO `detail_pemeriksaan_rawatjalan` (`no_periksa_rawat_jalan`, `id_penyakit`, `id_tindakan`, `biaya_tindakan`) VALUES
(1, 1233, 1222, 35000);

-- --------------------------------------------------------

--
-- Table structure for table `detail_penyakit`
--

DROP TABLE IF EXISTS `detail_penyakit`;
CREATE TABLE IF NOT EXISTS `detail_penyakit` (
  `no_urut` int(11) NOT NULL,
  `id_penyakit` int(11) NOT NULL,
  `no_periksa_rawat_inap` int(11) NOT NULL,
  `kondisi_pasien` varchar(50) NOT NULL,
  PRIMARY KEY (`no_urut`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `detail_resep_rawat_inap`
--

DROP TABLE IF EXISTS `detail_resep_rawat_inap`;
CREATE TABLE IF NOT EXISTS `detail_resep_rawat_inap` (
  `id_resep_rawat_inap` int(11) NOT NULL,
  `id_obat` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga_obat` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_resep_rawat_jalan`
--

DROP TABLE IF EXISTS `detail_resep_rawat_jalan`;
CREATE TABLE IF NOT EXISTS `detail_resep_rawat_jalan` (
  `id_resep_rawat_jalan` int(11) NOT NULL,
  `id_obat` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga_obat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `detail_resep_rawat_jalan`
--

INSERT INTO `detail_resep_rawat_jalan` (`id_resep_rawat_jalan`, `id_obat`, `jumlah`, `harga_obat`) VALUES
(1234, 212121, 1, 1221),
(1234, 212121, 7, 1221),
(1234, 212121, 7, 1221),
(1234, 1122, 6, 7500);

-- --------------------------------------------------------

--
-- Table structure for table `dokter`
--

DROP TABLE IF EXISTS `dokter`;
CREATE TABLE IF NOT EXISTS `dokter` (
  `id_dokter` int(11) NOT NULL,
  `nama_dokter` varchar(100) NOT NULL,
  `id_poli` int(11) NOT NULL,
  `spesialis` varchar(50) NOT NULL,
  PRIMARY KEY (`id_dokter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dokter`
--

INSERT INTO `dokter` (`id_dokter`, `nama_dokter`, `id_poli`, `spesialis`) VALUES
(112233, 'Dokter Mata', 112, 'Mata');

-- --------------------------------------------------------

--
-- Table structure for table `obat`
--

DROP TABLE IF EXISTS `obat`;
CREATE TABLE IF NOT EXISTS `obat` (
  `id_obat` int(11) NOT NULL,
  `nama_obat` varchar(50) NOT NULL,
  `stok_obat` int(100) NOT NULL,
  `expired_obat` date NOT NULL,
  `komposisi` varchar(50) NOT NULL,
  `kegunaan` varchar(100) NOT NULL,
  `cara_penggunaan` text NOT NULL,
  `dosis` varchar(50) NOT NULL,
  `jenis_obat` varchar(50) NOT NULL,
  `harga_obat` int(11) NOT NULL,
  PRIMARY KEY (`id_obat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `obat`
--

INSERT INTO `obat` (`id_obat`, `nama_obat`, `stok_obat`, `expired_obat`, `komposisi`, `kegunaan`, `cara_penggunaan`, `dosis`, `jenis_obat`, `harga_obat`) VALUES
(1122, 'Bodrexxx', 1176, '2023-01-31', 'Isinya Bodrex', 'Sakit Kepala', '                                                        diminum setelah makan\r\n                                                                                                                                                                ', '3 x Sehari Setelah Makan', 'Tablet', 7500),
(1234, 'Paramex', 87, '2023-02-04', 'Isinya Paramex', 'Sakit Kepala', 'Diminum setelah makan', '3x sehari setelah makan', 'Tablet', 5500),
(12312, 'asdasd', 1189, '2023-02-09', 'asdasd', 'dsadas', 'dsadsa', 'adsad', 'Tablet', 2112),
(212121, 'asdasd', 1083, '2023-01-31', 'Isinya Bodrex', 'asdasd', 'sadas', '122', 'Tablet', 1221);

-- --------------------------------------------------------

--
-- Table structure for table `pasien`
--

DROP TABLE IF EXISTS `pasien`;
CREATE TABLE IF NOT EXISTS `pasien` (
  `id_pasien` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pasien` varchar(100) NOT NULL,
  `jk_pasien` varchar(20) NOT NULL,
  `tgl_lahir_pasien` date NOT NULL,
  `pekerjaan_pasien` varchar(100) NOT NULL,
  `alamat_pasien` varchar(100) NOT NULL,
  `telepon_pasien` varchar(15) NOT NULL,
  `wali_pasien` varchar(100) NOT NULL,
  `hub_wali_pasien` varchar(50) NOT NULL,
  PRIMARY KEY (`id_pasien`)
) ENGINE=InnoDB AUTO_INCREMENT=224 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pasien`
--

INSERT INTO `pasien` (`id_pasien`, `nama_pasien`, `jk_pasien`, `tgl_lahir_pasien`, `pekerjaan_pasien`, `alamat_pasien`, `telepon_pasien`, `wali_pasien`, `hub_wali_pasien`) VALUES
(111, 'Ahmadul Huda', 'Laki-laki', '2012-04-11', 'Wirausaha', 'Singosari', '0897666', 'Wali Pasien 1 ', 'Orang Tua'),
(222, 'Huda Ahmadul', 'Laki-laki', '1998-01-03', 'Nganggur', 'Ga ada', '0128989', 'Asddd', 'Orang Tua'),
(223, 'Goldaaa', 'Laki-laki', '1998-01-11', 'PNS', 'Dimana mana', '12333', 'Goldis', 'Istri');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

DROP TABLE IF EXISTS `pegawai`;
CREATE TABLE IF NOT EXISTS `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(100) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`) VALUES
(123, 'Aldiansa\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `pemeriksaan_rawat_inap`
--

DROP TABLE IF EXISTS `pemeriksaan_rawat_inap`;
CREATE TABLE IF NOT EXISTS `pemeriksaan_rawat_inap` (
  `no_periksa_rawat_inap` int(11) NOT NULL AUTO_INCREMENT,
  `no_reg_rawat_inap` int(11) NOT NULL,
  `id_dokter` int(11) NOT NULL,
  `id_pasien` int(11) NOT NULL,
  `id_resep` int(11) NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `total_biaya` int(100) NOT NULL,
  PRIMARY KEY (`no_periksa_rawat_inap`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pemeriksaan_rawat_inap`
--

INSERT INTO `pemeriksaan_rawat_inap` (`no_periksa_rawat_inap`, `no_reg_rawat_inap`, `id_dokter`, `id_pasien`, `id_resep`, `id_ruang`, `total_biaya`) VALUES
(2, 1, 112233, 222, 1122, 1122, 35000),
(3, 2, 112233, 223, 1122, 1122, 70000);

-- --------------------------------------------------------

--
-- Table structure for table `pemeriksaan_rawat_jalan`
--

DROP TABLE IF EXISTS `pemeriksaan_rawat_jalan`;
CREATE TABLE IF NOT EXISTS `pemeriksaan_rawat_jalan` (
  `no_periksa_rawat_jalan` int(11) NOT NULL AUTO_INCREMENT,
  `no_reg_rawat_jalan` int(11) NOT NULL,
  `id_dokter` int(11) NOT NULL,
  `total_biaya` int(100) NOT NULL,
  PRIMARY KEY (`no_periksa_rawat_jalan`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pemeriksaan_rawat_jalan`
--

INSERT INTO `pemeriksaan_rawat_jalan` (`no_periksa_rawat_jalan`, `no_reg_rawat_jalan`, `id_dokter`, `total_biaya`) VALUES
(1, 1, 112233, 35000);

-- --------------------------------------------------------

--
-- Table structure for table `penyakit`
--

DROP TABLE IF EXISTS `penyakit`;
CREATE TABLE IF NOT EXISTS `penyakit` (
  `id_penyakit` int(11) NOT NULL,
  `nama_penyakit` varchar(50) NOT NULL,
  PRIMARY KEY (`id_penyakit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penyakit`
--

INSERT INTO `penyakit` (`id_penyakit`, `nama_penyakit`) VALUES
(1122, 'Batuk'),
(1233, 'Pilek');

-- --------------------------------------------------------

--
-- Table structure for table `poli`
--

DROP TABLE IF EXISTS `poli`;
CREATE TABLE IF NOT EXISTS `poli` (
  `id_poli` int(11) NOT NULL,
  `nama_poli` varchar(20) NOT NULL,
  PRIMARY KEY (`id_poli`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `poli`
--

INSERT INTO `poli` (`id_poli`, `nama_poli`) VALUES
(22, 'Kulitt'),
(112, 'Mata');

-- --------------------------------------------------------

--
-- Table structure for table `registrasi_rawat_inap`
--

DROP TABLE IF EXISTS `registrasi_rawat_inap`;
CREATE TABLE IF NOT EXISTS `registrasi_rawat_inap` (
  `no_reg_rawat_inap` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_reg_rawat_inap` date NOT NULL,
  `id_pasien` int(11) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  PRIMARY KEY (`no_reg_rawat_inap`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `registrasi_rawat_inap`
--

INSERT INTO `registrasi_rawat_inap` (`no_reg_rawat_inap`, `tgl_reg_rawat_inap`, `id_pasien`, `keterangan`) VALUES
(1, '2023-01-11', 222, 'ASDD'),
(2, '2023-01-11', 223, 'asdd');

-- --------------------------------------------------------

--
-- Table structure for table `registrasi_rawat_jalan`
--

DROP TABLE IF EXISTS `registrasi_rawat_jalan`;
CREATE TABLE IF NOT EXISTS `registrasi_rawat_jalan` (
  `no_reg_rawat_jalan` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_reg_rawat_jalan` date NOT NULL,
  `id_pasien` int(11) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  PRIMARY KEY (`no_reg_rawat_jalan`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `registrasi_rawat_jalan`
--

INSERT INTO `registrasi_rawat_jalan` (`no_reg_rawat_jalan`, `tgl_reg_rawat_jalan`, `id_pasien`, `keterangan`) VALUES
(1, '2023-01-11', 111, 'asdd');

-- --------------------------------------------------------

--
-- Table structure for table `resep_rawat_inap`
--

DROP TABLE IF EXISTS `resep_rawat_inap`;
CREATE TABLE IF NOT EXISTS `resep_rawat_inap` (
  `id_resep_rawat_inap` int(11) NOT NULL,
  `tgl_resep_rawat_inap` date NOT NULL,
  `id_dokter` int(11) NOT NULL,
  `total_bayar` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id_resep_rawat_inap`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `resep_rawat_jalan`
--

DROP TABLE IF EXISTS `resep_rawat_jalan`;
CREATE TABLE IF NOT EXISTS `resep_rawat_jalan` (
  `id_resep_rawat_jalan` int(11) NOT NULL,
  `tgl_resep_rawat_jalan` date NOT NULL,
  `id_dokter` int(11) NOT NULL,
  `id_pasien` int(11) NOT NULL,
  `id_poli` int(11) NOT NULL,
  `total_bayar` int(100) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id_resep_rawat_jalan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `resep_rawat_jalan`
--

INSERT INTO `resep_rawat_jalan` (`id_resep_rawat_jalan`, `tgl_resep_rawat_jalan`, `id_dokter`, `id_pasien`, `id_poli`, `total_bayar`, `status`) VALUES
(1234, '2023-01-11', 112233, 111, 22, 63315, 'BELUM DISERAHKAN');

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

DROP TABLE IF EXISTS `ruang`;
CREATE TABLE IF NOT EXISTS `ruang` (
  `id_ruang` int(11) NOT NULL,
  `nama_ruang` varchar(50) NOT NULL,
  `kapasitas_ruang` varchar(30) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kapasitas_ruang`) VALUES
(1122, 'Mawar', '5');

-- --------------------------------------------------------

--
-- Table structure for table `tindakan`
--

DROP TABLE IF EXISTS `tindakan`;
CREATE TABLE IF NOT EXISTS `tindakan` (
  `id_tindakan` int(11) NOT NULL,
  `nama_tindakan` varchar(50) NOT NULL,
  `biaya_tindakan` int(11) NOT NULL,
  PRIMARY KEY (`id_tindakan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tindakan`
--

INSERT INTO `tindakan` (`id_tindakan`, `nama_tindakan`, `biaya_tindakan`) VALUES
(1222, 'Suntik', 35000);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
