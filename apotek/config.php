<?php
require_once '../koneksi.php';

function query($query)
{
    global $koneksi;
    $result = mysqli_query($koneksi, $query);
    $rows = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }

    return $rows;
}

// Jumlah Data
function rows($query)
{
    global $koneksi;
    $result = mysqli_query($koneksi, $query);
    $rows = mysqli_num_rows($result);
    return $rows;
}

// Obat
function tambahObat($data){

    global $koneksi;

    $id_obat = $data["id_obat"];
    $nama_obat = $data["nama_obat"];
    $stok_obat = $data["stok_obat"];
    $expired_obat = $data["expired_obat"];
    $komposisi = $data["komposisi"];
    $kegunaan = $data["kegunaan"];
    $cara_penggunaan = $data["cara_penggunaan"];
    $dosis = $data["dosis"];
    $jenis_obat = $data["jenis_obat"];
    $harga_obat = $data["harga_obat"];


        mysqli_query($koneksi,"INSERT INTO obat VALUES(
            '$id_obat',
            '$nama_obat',
            '$stok_obat',
            '$expired_obat',
            '$komposisi',
            '$kegunaan',
            '$cara_penggunaan',
            '$dosis',
            '$jenis_obat',
            '$harga_obat')");
        return mysqli_affected_rows($koneksi);
}

function updateObat($data){
    global $koneksi;

    $id_obat = $data["id_obat"];
    $nama_obat = $data["nama_obat"];
    $stok_obat = $data["stok_obat"];
    $expired_obat = $data["expired_obat"];
    $komposisi = $data["komposisi"];
    $kegunaan = $data["kegunaan"];
    $cara_penggunaan = $data["cara_penggunaan"];
    $dosis = $data["dosis"];
    $jenis_obat = $data["jenis_obat"];
    $harga_obat = $data["harga_obat"];

    mysqli_query($koneksi,"UPDATE obat SET
        nama_obat = '$nama_obat',
        stok_obat = '$stok_obat',
        expired_obat = '$expired_obat',
        komposisi = '$komposisi',
        kegunaan = '$kegunaan',
        cara_penggunaan = '$cara_penggunaan',
        dosis = '$dosis',
        jenis_obat = '$jenis_obat',
        harga_obat = '$harga_obat'
        WHERE id_obat = '$id_obat'");

    return mysqli_affected_rows($koneksi);
}

function hapusObat($id) {
    global $koneksi;

    mysqli_query($koneksi, "DELETE FROM obat WHERE id_obat = $id");
    return mysqli_affected_rows($koneksi);

}

function addToCart($data) 
{
    global $koneksi;
    $id_obat = $data["id_obat"];
    $id_pegawai = $data["id_pegawai"];
    $banyak = $data["banyak"];

    $obat = query("SELECT * FROM obat WHERE id_obat = $id_obat")[0];

    $total = $banyak * $obat["harga_obat"];

    $queryCart = "INSERT INTO carts
                VALUES
                (NULL, '$id_pegawai', '$id_obat', '$banyak', '$total')
            ";
    
    mysqli_query($koneksi, $queryCart);

    $stockObat=  $obat["stok_obat"] - $banyak;

    $queryObat = "UPDATE obat SET
                        stok_obat = '$stockObat'
                    WHERE id_obat = $id_obat
                    ";
    mysqli_query($koneksi, $queryObat);

    return mysqli_affected_rows($koneksi);

}

function deleteProductAtCart($data)
{
    global $koneksi;
    $id_carts = $data["id_carts"];
    mysqli_query($koneksi, "DELETE FROM carts WHERE id_carts = $id_carts");
    return mysqli_affected_rows($koneksi);
}



function submitResep($data)
{
    global $koneksi;
    $id = $data["id"];
  $id_pegawai = $data["id_pegawai"];
//   $tanggal = $data["tanggal"];
//   $id_dokter = $data["id_dokter"];
//   $id_poli = $data["id_poli"];
//   $id_pasien = $data["id_pasien"];
  $total_bayar = $data["total_bayar"];
  $resep = $data["resep"];
  // $status = "BELUM DISERAHKAN";
  if ($total_bayar == 0) {
      echo "<script>
              alert('Pilih Produk Terlebih dahulu');
          </script>";
      return false;
  }

  if($resep==1){
      $queryResep = "UPDATE resep_rawat_inap SET total_bayar = $total_bayar 
      WHERE id_resep_rawat_inap = '$id'
      ";
      mysqli_query($koneksi, $queryResep);
      $carts = mysqli_query($koneksi,"SELECT * FROM carts INNER JOIN obat ON carts.id_obat = obat.id_obat WHERE id_pegawai = '$id_pegawai'");
    //   $dataResep = mysqli_query($koneksi,"SELECT * FROM resep_rawat_inap WHERE id_resep_rawat_inap = $id");
      foreach($carts as $cart){
        $id_obat = $cart['id_obat'];
          $harga_obat = $cart['harga_obat'];
          $banyak = $cart['banyak'];
          $queryDetail = "INSERT INTO detail_resep_rawat_inap VALUES ('$id','$id_obat','$banyak','$harga_obat')";
          mysqli_query($koneksi,$queryDetail);
        }
        mysqli_query($koneksi, "DELETE FROM carts WHERE id_pegawai = '$id_pegawai'");
        return mysqli_affected_rows($koneksi);
  }
//   else {
//     $queryResep = "INSERT INTO resep_rawat_jalan
//                       VALUES
//                       (NULL, '$tanggal', '$id_dokter', '$id_pasien', '$id_poli','$total_bayar','$status')
//       ";
//       mysqli_query($koneksi, $queryResep);
//       $carts = mysqli_query($koneksi,"SELECT * FROM carts INNER JOIN obat ON carts.id_obat = obat.id_obat WHERE id_pegawai = '$id_pegawai'");
//       $dataResep = mysqli_query($koneksi,"SELECT * FROM resep_rawat_jalan ORDER BY id_resep_rawat_jalan DESC LIMIT 1");
//       foreach($carts as $cart){
//         foreach($dataResep as $resep){
//           $id_resep_rawat_jalan = $resep["id_resep_rawat_jalan"];
//         }
//             $id_obat = $cart['id_obat'];
//           $harga_obat = $cart['harga_obat'];
//           $banyak = $cart['banyak'];
//           $queryDetail = "INSERT INTO detail_resep_rawat_jalan VALUES ('$id_resep_rawat_jalan','$id_obat','$banyak','$harga_obat')";
//           mysqli_query($koneksi,$queryDetail);
//         }
//         mysqli_query($koneksi, "DELETE FROM carts WHERE id_pegawai = '$id_pegawai'");
//         return mysqli_affected_rows($koneksi);
//   }
}

function updateResepRawatInap($data){
    global $koneksi;

    $id_resep_rawat_inap = $data["id_resep_rawat_inap"];
    $tgl_resep_rawat_inap = $data["tgl_resep_rawat_inap"];
    $id_dokter = $data["id_dokter"];
    $id_poli = $data["id_poli"];
    $id_pasien = $data["id_pasien"];
    $total_bayar = $data["total_bayar"];


    mysqli_query($koneksi,"UPDATE resep_rawat_inap SET
        tgl_resep_rawat_inap = '$tgl_resep_rawat_inap',
        id_dokter = '$id_dokter',
        id_poli = '$id_poli',
        id_pasien = '$id_pasien',
        total_bayar = '$total_bayar'
        WHERE id_resep_rawat_inap = '$id_resep_rawat_inap'");

    return mysqli_affected_rows($koneksi);
}

function updateStatusResepRawatInap($data){
    global $koneksi;

    $id_resep_rawat_inap = $data["id_resep_rawat_inap"];
    $tgl_resep_rawat_inap = $data["tgl_resep_rawat_inap"];
    $id_dokter = $data["id_dokter"];
    $total_bayar = $data["total_bayar"];
    $status = $data["status"];

    mysqli_query($koneksi,"UPDATE resep_rawat_inap SET
        tgl_resep_rawat_inap = '$tgl_resep_rawat_inap',
        id_dokter = '$id_dokter',
        total_bayar = '$total_bayar',
        status = '$status'
        WHERE id_resep_rawat_inap = '$id_resep_rawat_inap'");

    return mysqli_affected_rows($koneksi);
}

function updateResepRawatJalan($data){
    global $koneksi;

    $id_resep_rawat_jalan = $data["id_resep_rawat_jalan"];
    $tgl_resep_rawat_jalan = $data["tgl_resep_rawat_jalan"];
    $id_dokter = $data["id_dokter"];
    $id_poli = $data["id_poli"];
    $id_pasien = $data["id_pasien"];
    $total_bayar = $data["total_bayar"];


    mysqli_query($koneksi,"UPDATE resep_rawat_jalan SET
        tgl_resep_rawat_jalan = '$tgl_resep_rawat_jalan',
        id_dokter = '$id_dokter',
        id_poli = '$id_poli',
        id_pasien = '$id_pasien',
        total_bayar = '$total_bayar'
        WHERE id_resep_rawat_jalan = '$id_resep_rawat_jalan'");

    return mysqli_affected_rows($koneksi);
}

function updateStatusResepRawatJalan($data){
    global $koneksi;

    $id_resep_rawat_jalan = $data["id_resep_rawat_jalan"];
    $tgl_resep_rawat_jalan = $data["tgl_resep_rawat_jalan"];
    $id_dokter = $data["id_dokter"];
    $id_poli = $data["id_poli"];
    $id_pasien = $data["id_pasien"];
    $total_bayar = $data["total_bayar"];
    $status = $data["status"];

    mysqli_query($koneksi,"UPDATE resep_rawat_jalan SET
        tgl_resep_rawat_jalan = '$tgl_resep_rawat_jalan',
        id_dokter = '$id_dokter',
        id_poli = '$id_poli',
        id_pasien = '$id_pasien',
        total_bayar = '$total_bayar',
        status = '$status'
        WHERE id_resep_rawat_jalan = '$id_resep_rawat_jalan'");

    return mysqli_affected_rows($koneksi);
}

function hapusResepRawatInap($id) {
    global $koneksi;

    mysqli_query($koneksi, "DELETE FROM resep_rawat_inap WHERE id_resep_rawat_inap = $id");
    mysqli_query($koneksi, "DELETE FROM detail_resep_rawat_inap WHERE id_resep_rawat_inap = $id");
    return mysqli_affected_rows($koneksi);

}

function hapusResepRawatJalan($id) {
    global $koneksi;

    mysqli_query($koneksi, "DELETE FROM resep_rawat_jalan WHERE id_resep_rawat_jalan = $id");
    mysqli_query($koneksi, "DELETE FROM detail_resep_rawat_jalan WHERE id_resep_rawat_jalan = $id");
    return mysqli_affected_rows($koneksi);

}

// function test(){
//     global $koneksi;
//     mysqli_query($koneksi, "SELECT * FROM obat");
//     echo "Affected rows: " . mysqli_affected_rows($koneksi);
// }
?>