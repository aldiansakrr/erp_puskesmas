<?php 
include '../koneksi.php';
include 'config.php';
$page="Resep Rawat Inap";



if(isset($_POST['EditInap'])){
  if(updateResepRawatInap($_POST)>0){
    echo "<script>
            alert('Resep Berhasil Diubah');
            document.location.href = 'v_resep_rawatinap.php?pesan=edit';
          </script>";
  } else {
    echo "<script>
            alert('Resep Gagal Diubah');
            document.location.href = 'v_resep_rawatinap.php?pesan=gagal';
          </script>";
  }
}

if(isset($_POST['EditStatus'])){
  if(updateStatusResepRawatInap($_POST)>0){
  echo "<script>
            alert('Resep Berhasil Diubah');
            document.location.href = 'v_resep_rawatinap.php?pesan=edit';
          </script>";
  } else {
    echo "<script>
            alert('Resep Gagal Diubah');
            document.location.href = 'v_resep_rawatinap.php?pesan=gagal';
          </script>";
  }
}

  if(isset($_GET['id_resep_rawat_inap'])){
    $id_resep_rawat_inap = $_GET['id_resep_rawat_inap'];

    if (hapusResepRawatInap($id_resep_rawat_inap) > 0) {
    echo "<script>
            alert('Resep Berhasil Dihapus');
            document.location.href = 'v_resep_rawatinap.php?pesan=hapus';
        </script>";
      } else {
          echo "<script>
                  alert('Resep Gagal Dihapus');
                  document.location.href = 'v_resep_rawatinap.php?pesan=gagal';
              </script>";

      }
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $page; ?> | Apotek</title>

   <?php include ('css.php'); ?>

</head>
<?php
  session_start();
    if($_SESSION['id_pegawai']==""){

    header("location:login.php?pesan=belumlogin");
  }
  ?>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php include ('sidebar.php'); ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include ('navbar.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800"><?= $page ?></h1>
                       
                    </div>
                    <!-- Content Row -->
                    <div class="row">

                        <!-- Content Column -->
                        <div class="col-lg-12 mb-4">
                        <?php
          if(isset($_GET['pesan'])){
            if($_GET['pesan'] == "input"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Ditambahkan</h4>
            </div>
            </marquee>
              ";
            }else if($_GET['pesan'] == "edit"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Diedit</h4>
            </div>
            </marquee>
              ";
            }else if($_GET['pesan'] == "hapus"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Dihapus</h4>
            </div>
            </marquee>
              ";
            }
          }
          ?>
                            <!-- Project Card Example -->
                            <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">
                            </h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Tanggal Resep</th>
                                            <th>Dokter</th>
                                            <th>Total Bayar</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                          <th>#</th>
                                            <th>Tanggal Resep</th>
                                            <th>Dokter</th>
                                            <th>Total Bayar</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php
                                    $data = mysqli_query($koneksi,"SELECT * FROM resep_rawat_inap JOIN dokter ON resep_rawat_inap.id_dokter = dokter.id_dokter");
                                    $no=1;
                                    while($d=mysqli_fetch_array($data)){
                                    ?>
                                        <tr>
                                            <td><?= $no++; ?></td>
                                            <td><?= $d['tgl_resep_rawat_inap']; ?></td>
                                            <td><?= $d['nama_dokter']; ?></td>
                                            <td>Rp. <?= number_format($d['total_bayar']) ?></td>
                                            <td>
                                              <?php if($d['status']=="DISERAHKAN"){ ?>
                                            <span class="badge badge-success"><?= $d['status']; ?></span>
                                            <?php }else { ?>
                                              <span class="badge badge-danger"><?= $d['status']; ?></span>
                                              <?php } ?>  
                                            </td>
                                            <td>
                                            <div class="dropdown">
                                              <button class="btn btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Action
                                              </button>
                                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editresep<?php echo $no; ?>"><i class="fas fa fa-edit"></i> Edit</a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#deleteresep<?php echo $no; ?>"><i class="fas fa fa-trash"></i> Delete</a>
                                                <a class="dropdown-item" href="detail_rawatinap.php?id_resep_rawat_inap=<?= $d['id_resep_rawat_inap']; ?>"><i class="fas fa fa-info-circle"></i> Detail</a>
                                                <a class="dropdown-item" href="" data-toggle="modal" data-target="#penerimaanresep<?php echo $no; ?>"> <i class="fas fa fa-book"></i> Penerimaan Resep</a>
                                                <a class="dropdown-item" href="input_resep.php?id_resep_rawat_inap=<?= $d['id_resep_rawat_inap']; ?>"> <i class="fas fa fa-plus"></i> Proses Resep</a>
                                              </div>
                                            </div>  
                                          </td>
                                        </tr>

                                        <div class="modal fade" id="editresep<?= $no; ?>">
                                        <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title">Edit Data Resep Rawat Inap</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <form method="post" action="#">
                                            <?php
                                                $id_resep_rawat_inap = $d['id_resep_rawat_inap'];
                                                $query = "SELECT * FROM resep_rawat_inap WHERE id_resep_rawat_inap='$id_resep_rawat_inap'";
                                                $result = mysqli_query($koneksi,$query);
                                                while ($row = mysqli_fetch_assoc($result)){
                                            ?>
                                                <div class="card-body">
                                                <div class="form-group">
                                                    <label for="ID Obat">ID Resep Rawat Inap</label>
                                                    <input type="text" class="form-control" id="id_resep_rawat_inap" name="id_resep_rawat_inap" value="<?= $row['id_resep_rawat_inap']; ?>" readonly>
                                                </div>
                                                                        
                                                <div class="form-group">
                                                    <label for="Nama Obat">Tanggal Resep Rawat Inap</label>
                                                    <input type="date" class="form-control" id="tgl_resep_rawat_inap" name="tgl_resep_rawat_inap" value="<?= $row['tgl_resep_rawat_inap']; ?>" required>
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Dokter</label>
                                                    <select name="id_dokter" class="form-control">
                                                      <?php 
                                                      $dokters = mysqli_query($koneksi, "SELECT * FROM dokter");
                                                      while($dokter = mysqli_fetch_array($dokters)){
                                                      ?>
                                                      <option value="<?= $dokter['id_dokter']; ?>" <?php if($dokter['id_dokter']==$row['id_dokter']){ echo "selected"; } ?> ><?= $dokter['nama_dokter']; ?></option>
                                                      <?php } ?>
                                                    </select>
                                                </div>

                                                

                                                

                                                <div class="form-group">
                                                  <label for="Total Bayar">Total Bayar</label>
                                                  <input type="number" class="form-control" name="total_bayar" value="<?= $row['total_bayar']; ?>" readonly>
                                                </div>

                                                <div class="modal-footer justify-content-between">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary" name="EditInap">Simpan</button>
                                                </div>

                                                </div>
                                                <!-- /.card-body -->
                                                <?php } ?>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                    
                                    <div class="modal fade" id="deleteresep<?php echo $no; ?>">
                                                <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                    <h4 class="modal-title">Delete Data Resep</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    </div>
                                                    <div class="modal-body">
                                                    <h4 align="center" >Apakah anda yakin ingin menghapus resep dengan id <strong><?php echo $d['id_resep_rawat_inap'];?></strong> dan dengan total biaya <strong><?php echo $d['total_bayar']; ?></strong> ?</h4>
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                    <button id="nodelete" type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
                                                    <a href="v_resep_rawatinap.php?id_resep_rawat_inap=<?php echo $d['id_resep_rawat_inap']; ?>" class="btn btn-primary">Delete</a>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->
                                
                                            <div class="modal fade" id="penerimaanresep<?= $no; ?>">
                                        <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title">Penerimaan Resep</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <form method="post" action="#">
                                            <?php
                                                $id_resep_rawat_inap = $d['id_resep_rawat_inap'];
                                                $query = "SELECT * FROM resep_rawat_inap WHERE id_resep_rawat_inap='$id_resep_rawat_inap'";
                                                $result = mysqli_query($koneksi,$query);
                                                while ($row = mysqli_fetch_assoc($result)){
                                            ?>
                                                <div class="card-body">
                                                <div class="form-group">
                                                    <label for="">ID Resep Rawat Inap</label>
                                                    <input type="text" class="form-control" id="id_resep_rawat_inap" name="id_resep_rawat_inap" value="<?= $row['id_resep_rawat_inap']; ?>" readonly>
                                                </div>
                                                                        
                                                <div class="form-group">
                                                    <label for="">Tanggal Resep Rawat Inap</label>
                                                    <input type="date" class="form-control" id="tgl_resep_rawat_inap" name="tgl_resep_rawat_inap" value="<?= $row['tgl_resep_rawat_inap']; ?>" readonly>
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Dokter</label>
                                                    <select name="id_dokter" class="form-control" readonly>
                                                      <?php 
                                                      $dokters = mysqli_query($koneksi, "SELECT * FROM dokter");
                                                      while($dokter = mysqli_fetch_array($dokters)){
                                                      ?>
                                                      <option value="<?= $dokter['id_dokter']; ?>" <?php if($dokter['id_dokter']==$row['id_dokter']){ echo "selected"; } ?> ><?= $dokter['nama_dokter']; ?></option>
                                                      <?php } ?>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                  <label for="Total Bayar">Total Bayar</label>
                                                  <input type="number" class="form-control" name="total_bayar" value="<?= $row['total_bayar']; ?>" readonly>
                                                </div>
                                                 
                                                <div class="form-group">
                                                  <label for="Status">Status</label>
                                                  <select name="status" class="form-control">
                                                    <option value="">-- Pilih Status --</option>
                                                    <option value="BELUM DISERAHKAN">BELUM DISERAHKAN</option>
                                                    <option value="DISERAHKAN">DISERAHKAN</option>
                                                  </select>
                                                </div>
                    
                                                
                                                
                                                <div class="modal-footer justify-content-between">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary" name="EditStatus">Simpan</button>
                                                </div>

                                                </div>
                                                <!-- /.card-body -->
                                                <?php } ?>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->

                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                            
                    </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <?php include ('footer.php'); ?>

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    

   <?php include ('js.php'); ?>

</body>

</html>