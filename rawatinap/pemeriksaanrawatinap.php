<?php 
include '../koneksi.php';
$page="Pemeriksaan Rawat Inap"; 
if(isset($_POST['SimpanPemeriksaan'])){
    $no_periksa_rawat_inap = $_POST['no_periksa_rawat_inap'];
    $cekbiaya = mysqli_fetch_array(mysqli_query($koneksi,"SELECT * FROM pemeriksaan_rawat_inap WHERE no_periksa_rawat_inap = '$no_periksa_rawat_inap'"));
    $id_penyakit = $_POST['id_penyakit'];
    $id_tindakan = $_POST['id_tindakan'];
    $hargatindakan = mysqli_fetch_array(mysqli_query($koneksi,"SELECT * FROM tindakan WHERE id_tindakan = '$id_tindakan'"));
    $biaya_tindakan = $hargatindakan['biaya_tindakan'];
    mysqli_query($koneksi,"INSERT INTO detail_pemeriksaan_rawatinap VALUES(
        '$no_periksa_rawat_inap',
        '$id_penyakit',
        '$id_tindakan',
        '$biaya_tindakan'
        )");
    if($cekbiaya){
        $newbiaya = $biaya_tindakan + $cekbiaya['total_biaya'];
    }else{
        $newbiaya = $biaya_tindakan;
    }
    mysqli_query($koneksi,"UPDATE pemeriksaan_rawat_inap SET total_biaya = '$newbiaya' WHERE no_periksa_rawat_inap = '$no_periksa_rawat_inap'");
        header("location:pemeriksaanrawatinap.php?pesan=input");
  }

  if(isset($_POST['EditPemeriksaan'])){
    $no_periksa_rawat_inap = $_POST['no_periksa_rawat_inap'];
    $no_reg_rawat_inap = $_POST['no_reg_rawat_inap'];
    $id_dokter = $_POST['id_dokter'];
    $id_pasien = $_POST['id_pasien'];
    $id_resep = $_POST['id_resep'];
    $id_ruang = $_POST['id_ruang'];
    $total_biaya = $_POST['total_biaya'];
    mysqli_query($koneksi,"UPDATE pemeriksaan_rawat_inap SET
        no_reg_rawat_inap = '$no_reg_rawat_inap',
        id_dokter = '$id_dokter',
        id_pasien = '$id_pasien',
        id_resep = '$id_resep',
        id_ruang = '$id_ruang',
        total_biaya = '$total_biaya'
        WHERE no_periksa_rawat_inap = '$no_periksa_rawat_inap'");
    header("location:pemeriksaanrawatinap.php?pesan=edit");
  }

  if(isset($_GET['no_periksa_rawat_inap'])){
    $no_periksa_rawat_inap = $_GET['no_periksa_rawat_inap'];
    // $cek = mysqli_fetch_array(mysqli_query($koneksi,"SELECT * FROM pemeriksaan_rawat_inap WHERE no_perika_rawat_inap = '$no_periksa_rawat_inap'"));
    mysqli_query($koneksi,"DELETE FROM pemeriksaan_rawat_inap WHERE no_periksa_rawat_inap='$no_periksa_rawat_inap'");
    mysqli_query($koneksi,"DELETE FROM detail_pemeriksaan_rawatinap WHERE no_periksa_rawat_inap = '$no_periksa_rawat_inap'");
    header("location:pemeriksaanrawatinap.php?pesan=hapus");
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $page; ?> | Rawat Inap</title>

   <?php include ('css.php'); ?>

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php include ('sidebar.php'); ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include ('navbar.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800"><?= $page ?></h1>
                       
                    </div>
                    <!-- Content Row -->
                    <div class="row">

                        <!-- Content Column -->
                        <div class="col-lg-12 mb-4">
                        <?php
          if(isset($_GET['pesan'])){
            if($_GET['pesan'] == "input"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Ditambahkan</h4>
            </div>
            </marquee>
              ";
            }else if($_GET['pesan'] == "edit"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Diedit</h4>
            </div>
            </marquee>
              ";
            }else if($_GET['pesan'] == "hapus"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Dihapus</h4>
            </div>
            </marquee>
              ";
            }
          }
          ?>
                            <!-- Project Card Example -->
                            <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">
                                
                            </h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>No. Registrasi</th>
                                            <th>Dokter</th>
                                            <th>Pasien</th>
                                            <th>Resep</th>
                                            <th>Ruang</th>
                                            <th>Total Biaya</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>No. Registrasi</th>
                                            <th>Dokter</th>
                                            <th>Pasien</th>
                                            <th>Resep</th>
                                            <th>Ruang</th>
                                            <th>Total Biaya</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php
                                    $data = mysqli_query($koneksi,"SELECT * FROM pemeriksaan_rawat_inap JOIN dokter ON
                                    pemeriksaan_rawat_inap.id_dokter = dokter.id_dokter JOIN pasien ON
                                    pemeriksaan_rawat_inap.id_pasien = pasien.id_pasien JOIN ruang ON
                                    pemeriksaan_rawat_inap.id_ruang = ruang.id_ruang
                                    ");
                                    $no=1;
                                    while($d=mysqli_fetch_array($data)){
                                    ?>
                                        <tr>
                                            <td><?= $no++; ?></td>
                                            <td><?= $d['no_reg_rawat_inap']; ?></td>
                                            <td><?= $d['nama_dokter']; ?></td>
                                            <td><?= $d['nama_pasien']; ?></td>
                                            <td><?= $d['id_resep']; ?></td>
                                            <td><?= $d['nama_ruang']; ?></td>
                                            <td>Rp. <?= number_format($d['total_biaya']); ?></td>
                                            <td>
                                            <div class="dropdown">
                                              <button class="btn btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Action
                                              </button>
                                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#editpemeriksaan<?php echo $no; ?>"><i class="fas fa fa-edit"></i> Edit</a>
                                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#deletepemeriksaan<?php echo $no; ?>"><i class="fas fa fa-trash"></i> Delete</a>
                                                <a class="dropdown-item" href="detail_pemeriksaaninap.php?no_periksa_rawat_inap=<?= $d['no_periksa_rawat_inap']; ?>"><i class="fas fa fa-info-circle"></i> Detail</a>
                                                <a class="dropdown-item" href="" data-toggle="modal" data-target="#tambah<?php echo $no; ?>"> <i class="fas fa fa-book"></i> Tambah Tindakan</a>
                                              </div>
                                            </div>
                                                <!-- <a href="" data-toggle="modal" data-target="#editruang<?php echo $no; ?>" class="btn btn-outline-primary"><i class="fas fa fa-edit"></i> Edit</a>
                                                <a href="" data-toggle="modal" data-target="#deleteruang<?php echo $no; ?>" class="btn btn-outline-danger"><i class="fas fa fa-trash"></i> Delete</a>
                                                <a href="" data-toggle="modal" data-target="#tambah<?php echo $no; ?>" class="btn btn-outline-success"><i class="fas fa fa-edit"></i> Tambah Tindakan</a> -->
                                            </td>
                                        </tr>


                                        <div class="modal fade" id="tambah<?= $no; ?>">
                                        <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title">Tambah Tindakan Pemeriksaan</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <form method="post" action="#">
                                            <?php
                                                $no_periksa_rawat_inap = $d['no_periksa_rawat_inap'];
                                                $query = "SELECT * FROM pemeriksaan_rawat_inap WHERE no_periksa_rawat_inap='$no_periksa_rawat_inap'";
                                                $result = mysqli_query($koneksi,$query);
                                                while ($row = mysqli_fetch_assoc($result)){
                                            ?>
                                                <div class="card-body">
                                                <div class="form-group">
                                                    <label for="">No. Pemeriksaan</label>
                                                    <input type="text" class="form-control" id="no_periksa_rawat_inap" name="no_periksa_rawat_inap" value="<?= $row['no_periksa_rawat_inap']; ?>" readonly>
                                                </div>
                                                                        

                                                <div class="form-group">
                                                    <label for="">Penyakit</label>
                                                    <select name="id_penyakit" class="form-control">
                                                        <?php
                                                        $penyakits = mysqli_query($koneksi,"SELECT * FROM penyakit"); 
                                                        while($penyakit = mysqli_fetch_array($penyakits)) {
                                                        ?>
                                                        <option value="<?= $penyakit['id_penyakit']; ?>"  ><?= $penyakit['nama_penyakit']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Tindakan</label>
                                                    <select name="id_tindakan" class="form-control">
                                                        <?php
                                                        $tindakans = mysqli_query($koneksi,"SELECT * FROM tindakan"); 
                                                        while($tindakan = mysqli_fetch_array($tindakans)) {
                                                        ?>
                                                        <option value="<?= $tindakan['id_tindakan']; ?>"  ><?= $tindakan['nama_tindakan']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>

                                                
                                                
                                                
                                                <div class="modal-footer justify-content-between">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary" name="SimpanPemeriksaan">Simpan</button>
                                                </div>

                                                </div>
                                                <!-- /.card-body -->
                                                <?php } ?>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->

                                        <div class="modal fade" id="editpemeriksaan<?= $no; ?>">
                                        <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title">Edit Ruang</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <form method="post" action="#">
                                            <?php
                                                $no_periksa_rawat_inap = $d['no_periksa_rawat_inap'];
                                                $query = "SELECT * FROM pemeriksaan_rawat_inap WHERE no_periksa_rawat_inap='$no_periksa_rawat_inap'";
                                                $result = mysqli_query($koneksi,$query);
                                                while ($row = mysqli_fetch_assoc($result)){
                                            ?>
                                                <div class="card-body">
                                                <div class="form-group">
                                                    <label for="">No. Pemeriksaan</label>
                                                    <input type="text" class="form-control" id="no_periksa_rawat_inap" name="no_periksa_rawat_inap" value="<?= $row['no_periksa_rawat_inap']; ?>" readonly>
                                                </div>
                                                                        
                                                <div class="form-group">
                                                    <label for="">No. Regis</label>
                                                    <input type="text" class="form-control" id="no_reg_rawat_inap" name="no_reg_rawat_inap" value="<?= $row['no_reg_rawat_inap']; ?>" readonly>
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Dokter</label>
                                                    <select name="id_dokter" class="form-control">
                                                        <?php
                                                        $dokters = mysqli_query($koneksi,"SELECT * FROM dokter"); 
                                                        while($dokter = mysqli_fetch_array($dokters)) {
                                                        ?>
                                                        <option value="<?= $dokter['id_dokter']; ?>" <?php if($dokter['id_dokter']==$row['id_dokter']){ echo "selected"; } ?> ><?= $dokter['nama_dokter']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Pasien</label>
                                                    <input type="text" name="id_pasien" class="form-control" value="<?= $row['id_pasien']; ?>" readonly>
                                                </div>

                                                <div class="form-group">
                                                    <label for="">No. Resep</label>
                                                    <input type="text" name="id_resep" class="form-control" value="<?= $row['id_resep']; ?>" readonly>
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Ruang</label>
                                                    <select name="id_ruang" class="form-control">
                                                        <?php
                                                        $ruangs = mysqli_query($koneksi,"SELECT * FROM ruang"); 
                                                        while($ruang = mysqli_fetch_array($ruangs)) {
                                                        ?>
                                                        <option value="<?= $ruang['id_ruang']; ?>" <?php if($ruang['id_ruang']==$row['id_ruang']){ echo "selected"; } ?> ><?= $ruang['nama_ruang']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Total Biaya</label>
                                                    <input type="text" name="total_biaya" class="form-control" value="<?= $row['total_biaya']; ?>" readonly>
                                                </div>
                                                
                                                
                                                
                                                <div class="modal-footer justify-content-between">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary" name="EditPemeriksaan">Simpan</button>
                                                </div>

                                                </div>
                                                <!-- /.card-body -->
                                                <?php } ?>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                    
                                    <div class="modal fade" id="deletepemeriksaan<?php echo $no; ?>">
                                        <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title">Delete Ruang</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <h4 align="center" >Apakah anda yakin ingin menghapus ruang dengan id <strong><?php echo $d['no_periksa_rawat_inap'];?></strong> dan dengan nama <strong><?php echo $d['nama_dokter']; ?></strong> ?</h4>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                            <button id="nodelete" type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
                                            <a href="pemeriksaanrawatinap.php?no_periksa_rawat_inap=<?php echo $d['no_periksa_rawat_inap']; ?>" class="btn btn-primary">Delete</a>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->

                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                            
                    </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->
             

            <?php include ('footer.php'); ?>

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    

   <?php include ('js.php'); ?>

</body>

</html>