<?php 
include '../koneksi.php';
$page="Dokter"; 
if(isset($_POST['SimpanDokter'])){
    $id_dokter = $_POST['id_dokter'];
    $nama_dokter = $_POST['nama_dokter'];
    $id_poli = $_POST['id_poli'];
    $spesialis = $_POST['spesialis'];
    mysqli_query($koneksi,"INSERT INTO dokter VALUES(
        '$id_dokter',
        '$nama_dokter',
        '$id_poli',
        '$spesialis')");
    header("location:dokter.php?pesan=input");
  }

  if(isset($_POST['EditDokter'])){
    $id_dokter = $_POST['id_dokter'];
    $nama_dokter = $_POST['nama_dokter'];
    $id_poli = $_POST['id_poli'];
    $spesialis = $_POST['spesialis'];
    mysqli_query($koneksi,"UPDATE dokter SET
        nama_dokter = '$nama_dokter',
        id_poli = '$id_poli',
        spesialis = '$spesialis'
        WHERE id_dokter = '$id_dokter'");
    header("location:dokter.php?pesan=edit");
  }

  if(isset($_GET['id_dokter'])){
    $id_dokter = $_GET['id_dokter'];
  
    mysqli_query($koneksi,"DELETE FROM dokter WHERE id_dokter='$id_dokter'");
    header("location:dokter.php?pesan=hapus");
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $page; ?> | Rawat Jalan</title>

   <?php include ('css.php'); ?>

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php include ('sidebar.php'); ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include ('navbar.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800"><?= $page ?></h1>
                       
                    </div>
                    <!-- Content Row -->
                    <div class="row">

                        <!-- Content Column -->
                        <div class="col-lg-12 mb-4">
                        <?php
          if(isset($_GET['pesan'])){
            if($_GET['pesan'] == "input"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Ditambahkan</h4>
            </div>
            </marquee>
              ";
            }else if($_GET['pesan'] == "edit"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Diedit</h4>
            </div>
            </marquee>
              ";
            }else if($_GET['pesan'] == "hapus"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Dihapus</h4>
            </div>
            </marquee>
              ";
            }
          }
          ?>
                            <!-- Project Card Example -->
                            <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">
                                <a href="" class="btn btn-outline-primary btn-sm float-right" data-toggle="modal" data-target="#inputdokter"><i class="fas fa fa-plus"></i> Tambah Dokter</a>
                            </h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Dokter</th>
                                            <th>ID Poli</th>
                                            <th>Spesialis</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Dokter</th>
                                            <th>ID Poli</th>
                                            <th>Spesialis</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php
                                    $data = mysqli_query($koneksi,"SELECT * FROM dokter");
                                    $no=1;
                                    while($d=mysqli_fetch_array($data)){
                                    ?>
                                        <tr>
                                            <td><?= $no++; ?></td>
                                            <td><?= $d['nama_dokter']; ?></td>
                                            <td><?= $d['id_poli']; ?></td>
                                            <td><?= $d['spesialis']; ?></td>
                                            <td>
                                                <a href="" data-toggle="modal" data-target="#editdokter<?php echo $no; ?>" class="btn btn-outline-primary"><i class="fas fa fa-edit"></i> Edit</a>
                                                <a href="" data-toggle="modal" data-target="#deletedokter<?php echo $no; ?>" class="btn btn-outline-danger"><i class="fas fa fa-trash"></i> Delete</a>
                                            </td>
                                        </tr>

                                        <div class="modal fade" id="editdokter<?= $no; ?>">
                                        <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title">Edit Dokter</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <form method="post" action="#">
                                            <?php
                                                $id_dokter = $d['id_dokter'];
                                                $query = "SELECT * FROM dokter WHERE id_dokter='$id_dokter'";
                                                $result = mysqli_query($koneksi,$query);
                                                while ($row = mysqli_fetch_assoc($result)){
                                            ?>
                                                <div class="card-body">
                                                <div class="form-group">
                                                    <label for="ID Dokter">ID Dokter</label>
                                                    <input type="text" class="form-control" id="id_dokter" name="id_dokter" value="<?= $row['id_dokter']; ?>" readonly>
                                                </div>
                                                                        
                                                <div class="form-group">
                                                    <label for="Nama Dokter">Nama Dokter</label>
                                                    <input type="text" class="form-control" id="nama_dokter" name="nama_dokter" value="<?= $row['nama_dokter']; ?>" required>
                                                </div>

                                              <div class="form-group">
                                              <label for="ID Poli">ID Poli</label>
                                              <select name="id_poli" id="id_poli" class="form-control">
                                                <?php 
                                                  $polis = mysqli_query($koneksi,"SELECT * FROM poli");
                                                  while($poli = mysqli_fetch_array($polis)){
                                                ?>
                                                <option value="<?= $poli['id_poli']; ?>" <?php if($poli['id_poli']==$row['id_poli']){ echo "selected"; } ?> ><?= $poli['nama_poli']; ?></option>
                                                <?php } ?>
                                              </select>
                                            </div>

                                                <div class="form-group">
                                                    <label for="Spesialis">Spesialis</label>
                                                    <input type="text" name="spesialis" id="spesialis" class="form-control" value="<?= $row['spesialis']; ?>" required>
                                                </div>
                                                
                                                
                                                <div class="modal-footer justify-content-between">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary" name="EditDokter">Simpan</button>
                                                </div>

                                                </div>
                                                <!-- /.card-body -->
                                                <?php } ?>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                    
                                    <div class="modal fade" id="deletedokter<?php echo $no; ?>">
                                        <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title">Delete Dokter</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <h4 align="center" >Apakah anda yakin ingin menghapus dokter dengan id <strong><?php echo $d['id_dokter'];?></strong> dan dengan nama <strong><?php echo $d['nama_dokter']; ?></strong> ?</h4>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                            <button id="nodelete" type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
                                            <a href="dokter.php?id_dokter=<?php echo $d['id_dokter']; ?>" class="btn btn-primary">Delete</a>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->

                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                            
                    </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->
             
            <div class="modal fade" id="inputdokter">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Dokter</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
             <form method="post" action="#">
                <div class="card-body">
                  <div class="form-group">
                    <label for="ID Dokter">ID Dokter</label>
                    <input type="text" class="form-control" id="id_dokter" placeholder="Ex : 001" name="id_dokter" required>
                  </div>
                                        
                  <div class="form-group">
                    <label for="Nama Dokter">Nama Dokter</label>
                    <input type="text" class="form-control" id="nama_dokter" placeholder="Ex : dr. Cindi Undi" name="nama_dokter" required>
                  </div>
                  
                  <div class="form-group">
                    <label for="ID Poli">ID Poli</label>
                    <select name="id_poli" id="id_poli" class="form-control">
                      <?php 
                        $polis = mysqli_query($koneksi,"SELECT * FROM poli");
                        while($poli = mysqli_fetch_array($polis)){
                      ?>
                      <option value="<?= $poli['id_poli']; ?>"><?= $poli['nama_poli']; ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="Spesialis">Spesialis</label>
                    <input type="text" name="spesialis" id="spesialis" placeholder="Ex : Mata" class="form-control" required>
                  </div>
                  
                  <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary" name="SimpanDokter">Simpan</button>
                  </div>

                </div>
                <!-- /.card-body -->
                </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

            <?php include ('footer.php'); ?>

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    

   <?php include ('js.php'); ?>

</body>

</html>