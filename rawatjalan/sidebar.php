<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
    <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-stethoscope"></i>
    </div>
    <div class="sidebar-brand-text mx-3">RAWAT JALAN</div>
</a>

<!-- Divider -->
<hr class="sidebar-divider my-0">

<!-- Nav Item - Dashboard -->
<li class="nav-item <?php if($page=="Dashboard"){ echo "active"; } ?>">
    <a class="nav-link" href="index.php">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
    Interface
</div>

<li class="nav-item <?php if($page=="Registrasi Rawat Jalan"){ echo "active"; } ?>">
    <a class="nav-link" href="registrasirawatjalan.php">
        <i class="fas fa-user-plus"></i>
        <span>Registrasi Rawat Jalan</span></a>
</li>

<!-- Nav Item - Pasien -->
<li class="nav-item <?php if($page=="Pasien"){ echo "active"; } ?>">
    <a class="nav-link" href="pasien.php">
        <i class="fas fa-fw fa-user"></i>
        <span>Pasien</span></a>
</li>

<!-- Nav Item - Dokter -->
<li class="nav-item <?php if($page=="Dokter"){ echo "active"; } ?>">
    <a class="nav-link" href="dokter.php">
        <i class="fas fa-user-md"></i>
        <span>Dokter</span></a>
</li>

<!-- Nav Item - Tables -->
<li class="nav-item <?php if($page=="Pemeriksaan Rawat Jalan"){ echo "active"; } ?>">
    <a class="nav-link" href="pemeriksaanrawatjalan.php">
        <i class="fas fa-stethoscope"></i>
        <span>Pemeriksaan Rawat Jalan</span></a>
</li>

<!-- Nav Item - Penyakit -->
<li class="nav-item <?php if($page=="Penyakit"){ echo "active"; } ?>">
    <a class="nav-link" href="penyakit.php">
        <i class="fas fa-diagnoses"></i>
        <span>Penyakit</span></a>
</li>

<!-- Nav Item - Poli -->
<li class="nav-item <?php if($page=="Poli"){ echo "active"; } ?>">
    <a class="nav-link" href="poli.php">
        <i class="fas fa-medkit"></i>
        <span>Poli</span></a>
</li>

<!-- Nav Item - Penyakit -->
<li class="nav-item <?php if($page=="Tindakan"){ echo "active"; } ?>">
    <a class="nav-link" href="tindakan.php">
        <i class="fas fa-fill-drip"></i>
        <span>Tindakan</span></a>
</li>

<!-- Nav Item - Obat -->
<li class="nav-item <?php if($page=="Data Obat"){ echo "active"; } ?>">
    <a class="nav-link" href="dataobat.php">
        <i class="fas fa-capsules"></i>
        <span>Obat</span></a>
</li>



<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>



</ul>
<!-- End of Sidebar -->