<?php 
include '../koneksi.php';
include 'config.php';
$page="Data Obat";

if(isset($_POST['SimpanObat'])){
  if(tambahObat($_POST)>0){
    echo "<script>
            alert('Obat Berhasil Ditambah');
            document.location.href = 'dataobat.php?pesan=input';
          </script>";
  } else {
    echo "<script>
            alert('Obat Gagal Ditambah');
            document.location.href = 'dataobat.php?pesan=gagal';
          </script>";
  }
}

if(isset($_POST['EditObat'])){
  if(updateObat($_POST)>0){
    echo "<script>
            alert('Obat Berhasil Diubah');
            document.location.href = 'dataobat.php?pesan=edit';
          </script>";
  } else {
    echo "<script>
            alert('Obat Gagal Diubah');
            document.location.href = 'dataobat.php?pesan=gagal';
          </script>";
  }
}

  if(isset($_GET['id_obat'])){
    $id_obat = $_GET['id_obat'];

    if (hapusObat($id_obat) > 0) {
    echo "<script>
            alert('Obat Berhasil Dihapus');
            document.location.href = 'dataobat.php?pesan=hapus';
        </script>";
      } else {
          echo "<script>
                  alert('Obat Gagal Dihapus');
                  document.location.href = 'dataobat.php?pesan=gagal';
              </script>";

      }
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $page; ?> | Apotek</title>

   <?php include ('css.php'); ?>

</head>
<?php
  session_start();
    if($_SESSION['id_pegawai']==""){

    header("location:login.php?pesan=belumlogin");
  }
  ?>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php include ('sidebar.php'); ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <?php include ('navbar.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800"><?= $page ?></h1>
                       
                    </div>
                    <!-- Content Row -->
                    <div class="row">

                        <!-- Content Column -->
                        <div class="col-lg-12 mb-4">
                        <?php
          if(isset($_GET['pesan'])){
            if($_GET['pesan'] == "input"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Ditambahkan</h4>
            </div>
            </marquee>
              ";
            }else if($_GET['pesan'] == "edit"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Diedit</h4>
            </div>
            </marquee>
              ";
            }else if($_GET['pesan'] == "hapus"){
              echo "
              <marquee>
            <div class='alert alert-warning alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4><i class='icon fa fa-info'></i> Data Berhasil Dihapus</h4>
            </div>
            </marquee>
              ";
            }
          }
          ?>
                            <!-- Project Card Example -->
                            <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">
                                <a href="" class="btn btn-outline-primary btn-sm float-right" data-toggle="modal" data-target="#inputobat"><i class="fas fa fa-plus"></i> Tambah Data Obat</a>
                            </h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Obat</th>
                                            <th>Stok Obat</th>
                                            <th>Expired Obat</th>
                                            <th>Jenis Obat</th>
                                            <th>Harga Obat</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Obat</th>
                                            <th>Stok Obat</th>
                                            <th>Expired Obat</th>
                                            <th>Jenis Obat</th>
                                            <th>Harga Obat</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php
                                    $data = mysqli_query($koneksi,"SELECT * FROM obat");
                                    $no=1;
                                    while($d=mysqli_fetch_array($data)){
                                    ?>
                                        <tr>
                                            <td><?= $no++; ?></td>
                                            <td><?= $d['nama_obat']; ?></td>
                                            <td><?= $d['stok_obat']; ?></td>
                                            <td><?= $d['expired_obat']; ?></td>
                                            <td><?= $d['jenis_obat']; ?></td>
                                            <td><?= number_format($d['harga_obat']); ?></td>
                                            <td>
                                                <a href="" data-toggle="modal" data-target="#editobat<?php echo $no; ?>" class="btn btn-outline-primary"><i class="fas fa fa-edit"></i> Edit</a>
                                                <a href="" data-toggle="modal" data-target="#deleteobat<?php echo $no; ?>" class="btn btn-outline-danger"><i class="fas fa fa-trash"></i> Delete</a>
                                            </td>
                                        </tr>

                                        <div class="modal fade" id="editobat<?= $no; ?>">
                                        <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title">Edit Data Obat</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <form method="post" action="#">
                                            <?php
                                                $id_obat = $d['id_obat'];
                                                $query = "SELECT * FROM obat WHERE id_obat='$id_obat'";
                                                $result = mysqli_query($koneksi,$query);
                                                while ($row = mysqli_fetch_assoc($result)){
                                            ?>
                                                <div class="card-body">
                                                <div class="form-group">
                                                    <label for="ID Obat">ID Obat</label>
                                                    <input type="text" class="form-control" id="id_obat" name="id_obat" value="<?= $row['id_obat']; ?>" readonly>
                                                </div>
                                                                        
                                                <div class="form-group">
                                                    <label for="Nama Obat">Nama Obat</label>
                                                    <input type="text" class="form-control" id="nama_obat" name="nama_obat" value="<?= $row['nama_obat']; ?>" required>
                                                </div>

                                                <div class="form-group">
                                                    <label for="Stok Obat">Stok Obat</label>
                                                    <input type="number" class="form-control" id="stok_obat" name="stok_obat" value="<?= $row['stok_obat']; ?>" required>
                                                </div>

                                                <div class="form-group">
                                                    <label for="Expired Obat">Expired Obat</label>
                                                    <input type="date" name="expired_obat" id="expired_obat" class="form-control" value="<?= $row['expired_obat']; ?>">
                                                </div>

                                                <div class="form-group">
                                                    <label for="Komposisi">Komposisi</label>
                                                    <input type="text" name="komposisi" id="komposisi" class="form-control" value="<?= $row['komposisi']; ?>" required>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="Kegunaan">Kegunaan</label>
                                                    <input type="text" name="kegunaan" id="kegunaan" class="form-control" value="<?= $row['kegunaan']; ?>" required>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="Cara Penggunaan">Cara Penggunaan</label>
                                                    <textarea name="cara_penggunaan" id="cara_penggunaan" class="form-control">
                                                        <?= $row['cara_penggunaan']; ?>
                                                    </textarea>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="Dosis">Dosis</label>
                                                    <input type="text" name="dosis" id="dosis" class="form-control" value="<?= $row['dosis']; ?>" required>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="Jenis Obat">Jenis Obat</label>
                                                    <select name="jenis_obat" id="jenis_obat" class="form-control">
                                                        <option value="Tablet" <?php if($row['jenis_obat']=="Tablet"){ echo "selected"; } ?> >Tablet</option>
                                                        <option value="Cair" <?php if($row['jenis_obat']=="Cair"){ echo "selected"; } ?> >Cair</option>
                                                    </select>
                                                </div>                      

                                                <div class="form-group">
                                                    <label for="Harga Obat">Harga Obat</label>
                                                    <input type="number" name="harga_obat" class="form-control" id="harga_obat" value="<?= $row['harga_obat']; ?>" required>
                                                </div>
                                                
                                                
                                                <div class="modal-footer justify-content-between">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary" name="EditObat">Simpan</button>
                                                </div>

                                                </div>
                                                <!-- /.card-body -->
                                                <?php } ?>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                    
                                    <div class="modal fade" id="deleteobat<?php echo $no; ?>">
                                        <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title">Delete Data Obat</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <h4 align="center" >Apakah anda yakin ingin menghapus obat dengan id <strong><?php echo $d['id_obat'];?></strong> dan dengan nama <strong><?php echo $d['nama_obat']; ?></strong> ?</h4>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                            <button id="nodelete" type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
                                            <a href="dataobat.php?id_obat=<?php echo $d['id_obat']; ?>" class="btn btn-primary">Delete</a>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->

                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                            
                    </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->
             
            <div class="modal fade" id="inputobat">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Data Obat</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
             <form method="post" action="#">
                <div class="card-body">
                  <div class="form-group">
                    <label for="ID Obat">ID Obat</label>
                    <input type="text" class="form-control" id="id_obat" placeholder="Ex : 12332" name="id_obat" required>
                  </div>
                                        
                  <div class="form-group">
                    <label for="Nama Obat">Nama Obat</label>
                    <input type="text" class="form-control" id="nama_obat" placeholder="Ex : Bodrex" name="nama_obat" required>
                  </div>

                  <div class="form-group">
                    <label for="Stok Obat">Stok Obat</label>
                    <input type="number" class="form-control" id="stok_obat" placeholder="Ex : 22" name="stok_obat" required>
                  </div>

                  <div class="form-group">
                    <label for="Expired Obat">Expired Obat</label>
                    <input type="date" name="expired_obat" id="expired_obat" class="form-control">
                  </div>

                  <div class="form-group">
                    <label for="Komposisi">Komposisi</label>
                    <input type="text" name="komposisi" id="komposisi" class="form-control" required>
                  </div>
                  
                  <div class="form-group">
                    <label for="Kegunaan">Kegunaan</label>
                    <input type="text" name="kegunaan" id="kegunaan" class="form-control" required>
                  </div>
                  
                  <div class="form-group">
                    <label for="Cara Penggunaan">Cara Penggunaan</label>
                    <textarea name="cara_penggunaan" id="cara_penggunaan" cols="" rows="" class="form-control"></textarea>
                  </div>
                  
                  <div class="form-group">
                    <label for="Dosis">Dosis</label>
                    <input type="text" name="dosis" id="dosis" class="form-control" required>
                  </div>
                  
                  <div class="form-group">
                    <label for="Jenis Obat">Jenis Obat</label>
                    <select name="jenis_obat" id="jenis_obat" class="form-control">
                        <option value="">-- Pilih Jenis Obat --</option>
                        <option value="Tablet">Tablet</option>
                        <option value="Cair">Cair</option>
                    </select>
                  </div>                      

                  <div class="form-group">
                    <label for="Harga Obat">Harga Obat</label>
                    <input type="number" name="harga_obat" class="form-control" id="harga_obat" placeholder="Ex : 2500" required>
                  </div>
                  
                  
                  <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary" name="SimpanObat">Simpan</button>
                  </div>

                </div>
                <!-- /.card-body -->
                </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

            <?php include ('footer.php'); ?>

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    

   <?php include ('js.php'); ?>

</body>

</html>